package com.example.team12.virtualteacher.services;

import com.example.team12.virtualteacher.exceptions.DuplicateEntityException;
import com.example.team12.virtualteacher.exceptions.EntityNotFoundException;
import com.example.team12.virtualteacher.exceptions.UnauthorizedOperationException;
import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.models.UserRole;
import com.example.team12.virtualteacher.models.enums.CourseStatusType;
import com.example.team12.virtualteacher.repositories.contracts.CourseRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class CourseServiceImplTests {
    @Mock
    CourseRepository mockRepository;

    @InjectMocks
    CourseServiceImpl courseService;

    @Test
    public void getCourses_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        courseService.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnUser_when_matchExist() {
        Course mockCourse = createMockCourse();

        Mockito.when(courseService.getById(mockCourse.getId()))
                .thenReturn(mockCourse);

        Course result = courseService.getById(mockCourse.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCourse.getTitle(), result.getTitle()),
                () -> Assertions.assertEquals(mockCourse.getDescription(), result.getDescription()),
                () -> Assertions.assertEquals(mockCourse.getTopic(), result.getTopic()),
                () -> Assertions.assertEquals(mockCourse.getId(), result.getId())
        );
    }

    @Test
    public void create_should_throw_when_courseWithSameTitleExists() {
        Course mockCourse = createMockCourse();
        User mockUser = createMockUser();

        Mockito.when(courseService.getCourseByTitle(mockCourse.getTitle()))
                .thenReturn(mockCourse);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> courseService.create(mockCourse, mockUser));
    }

    @Test
    public void create_should_callRepository_when_courseWithSameTitleDoesNotExist() {
        Course mockCourse = createMockCourse();
        User mockUser = createMockUser();

        Mockito.when(mockRepository.getByField("title", mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        courseService.create(mockCourse, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockCourse);
    }

    @Test
    public void update_should_throwException_when_userIsNotCreatorOrAdmin() {
        Course mockCourse = createMockCourse();
        User mockUpdator = createMockUser();
        mockUpdator.getRole().setName("Normal");

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> courseService.update(mockCourse, mockUpdator));
    }

    @Test
    void update_should_callRepository_when_userIsAdmin() {
        Course mockCourse = createMockCourse();
        User mockUpdator = createMockUser();

        Mockito.when(mockRepository.getByField("title", Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockCourse);
    }

    @Test
    public void update_should_throwException_when_titleIsTaken() {
        Course mockCourse = createMockCourse();
        User mockUpdator = createMockUser();
        Course anotherMockCourse = createMockCourse();

        anotherMockCourse.setId(2);

        Mockito.when(mockRepository.getByField("title", mockCourse.getTitle()))
                .thenReturn(anotherMockCourse);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> courseService.update(mockCourse, mockUpdator));
    }

    @Test
    public void update_should_callRepository_when_tryingToUpdateExistingCourse() {
        Course mockCourse = createMockCourse();
        User mockUpdator = createMockUser();
        mockUpdator.setId(2);

        Mockito.when(mockRepository.getByField("title", mockCourse.getTitle()))
                .thenReturn(mockCourse);

        courseService.update(mockCourse, mockUpdator);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockCourse);
    }

    @Test
    void delete_should_callRepository_when_initiatorIsAdmin() {
        Course mockCourse = createMockCourse();
        User mockUpdator = createMockUser();
        mockUpdator.setId(2);

        courseService.delete(mockCourse.getId(), mockUpdator);

        Mockito.verify(mockRepository, Mockito.times(1)).
                delete(mockCourse.getId());
    }

    @Test
    void delete_should_throw_when_initiatorIsNotAdmin() {
        Course mockCourse = createMockCourse();
        User updator = createMockUser();
        updator.getRole().setName("Normal");
        updator.setId(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> courseService.delete(mockCourse.getId(), updator));
    }

    private Course createMockCourse() {
        Course mockCourse = new Course();
        mockCourse.setTitle("test");
        mockCourse.setDescription("test");
        mockCourse.setTopic("test");
        mockCourse.setStartDate(LocalDate.now());
        mockCourse.setId(1);
        mockCourse.setStatus(CourseStatusType.PUBLISHED);


        return mockCourse;
    }

    private User createMockUser() {
        User mockUser = new User();
        mockUser.setFirstName("test");
        mockUser.setLastName("test");
        mockUser.setEmail("test");
        mockUser.setId(1);

        UserRole role = new UserRole();
        role.setName("Admin");

        mockUser.setRole(role);

        return mockUser;
    }
}
