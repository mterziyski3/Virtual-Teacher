package com.example.team12.virtualteacher;

import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.models.UserRole;
import com.example.team12.virtualteacher.models.enums.CourseStatusType;

import java.time.LocalDate;

public class Helpers {

    public static User createMockAdmin() {
        return createMockUser("Admin");
    }

    public static User createMockStudent() {
        return createMockUser("Student");
    }

    public static User createMockTeacher() {
        return createMockUser("Teacher");
    }

    public static User createMockUser(String role) {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setEmail("MockUsername@abv.bg");
        mockUser.setPassword("MockPassword");
        mockUser.setEmail("gosho39@abv.bg");
        mockUser.setRole((createMockRole(role)));

        return mockUser;
    }

    public static UserRole createMockRole(String role) {
        var mockRole = new UserRole();
        mockRole.setId(1);
        mockRole.setName(role);
        return mockRole;
    }

    public static Course createMockCourse() {
        Course mockCourse = new Course();
        mockCourse.setTitle("mockCourse");
        mockCourse.setDescription("mockCourse");
        mockCourse.setTopic("mockCourse");
        mockCourse.setStartDate(LocalDate.now());
        mockCourse.setId(1);
        mockCourse.setStatus(CourseStatusType.PUBLISHED);

        return mockCourse;
    }
}
