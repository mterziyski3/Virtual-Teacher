package com.example.team12.virtualteacher.services;

import com.example.team12.virtualteacher.exceptions.DuplicateEntityException;
import com.example.team12.virtualteacher.exceptions.EntityNotFoundException;
import com.example.team12.virtualteacher.exceptions.UnauthorizedOperationException;
import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.models.UserRole;
import com.example.team12.virtualteacher.repositories.contracts.CourseRepository;
import com.example.team12.virtualteacher.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static com.example.team12.virtualteacher.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {


    @Mock
    UserRepository mockRepository;

    @Mock
    CourseRepository mockCourseRepository;
    @InjectMocks
    UserServiceImpl service;

    @Test
    public void filter_Should_CallRepository() {

        Mockito.when(mockRepository.filter(Mockito.any()))
                .thenReturn(new ArrayList<>());

        service.filter(Mockito.any(), Mockito.any());

        Mockito.verify(mockRepository, Mockito.times(1))
                .filter(Mockito.any());
    }

    @Test
    public void getAllUsers_should_throw_when_userIsNotAdmin() {
        User mockUser = createMockStudent();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getUsers(mockUser));
    }

    @Test
    public void getAllUser_should_callRepository() {
        User mockUser = createMockAdmin();

        Mockito.when(mockRepository.getUsers())
                .thenReturn(new ArrayList<>());

        service.getUsers(mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .getUsers();
    }

    @Test
    public void getById_should_returnUser_when_matchExist() {
        User mockUser = createMockAdmin();

        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        User result = service.getUserById(mockUser.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getFirstName(), result.getFirstName()),
                () -> Assertions.assertEquals(mockUser.getLastName(), result.getLastName()),
                () -> Assertions.assertEquals(mockUser.getPassword(), result.getPassword()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail())
        );
    }

    @Test
    public void getAllStudents_should_throw_when_userIsNotAdmin() {
        User mockUser = createMockStudent();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getAllStudents(mockUser));
    }

    @Test
    public void getAllStudents_should_callRepository() {
        User mockUser = createMockAdmin();

        Mockito.when(mockRepository.getAllStudents())
                .thenReturn(new ArrayList<>());

        service.getAllStudents(mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllStudents();
    }

    @Test
    public void getTeacherCreatedCourses_should_throw_when_userIsNotAdmin() {
        User mockUser = createMockStudent();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getTeacherCreatedCourses(mockUser, mockUser.getId()));
    }

    @Test
    public void getTeacherCreatedCourses_should_callRepository() {
        User mockUser = createMockAdmin();

        Mockito.when(mockRepository.getTeacherCreatedCourses(mockUser.getId()))
                .thenReturn(new ArrayList<>());

        service.getTeacherCreatedCourses(mockUser, mockUser.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getTeacherCreatedCourses(mockUser.getId());
    }

//    @Test
//    public void getStudentEnrolledCourses_should_throw_when_userIsNotSameStudent() {
//        User mockUser = createMockStudent();
//        User anotherMockUser = createMockStudent();
//        anotherMockUser.setId(2);
//
//        Assertions.assertThrows(UnauthorizedOperationException.class,
//                () -> service.getStudentEnrolledCourses(mockUser, anotherMockUser.getId()));
//    }

//    @Test
//    public void getStudentEnrolledCourses_should_callRepository() {
//        User mockUser = createMockAdmin();
//
//        Mockito.when(mockRepository.getById(mockUser.getId()))
//                .thenReturn(mockUser);
//
//        service.getStudentEnrolledCourses(mockUser, mockUser.getId());
//
//        Mockito.verify(service.getStudentEnrolledCourses(mockUser, mockUser.getId())).stream().collect(Collectors.toList());
//    }

    @Test
    public void getStudentEnrolledLectures_should_throw_when_userIsNotSameStudent() {
        User mockUser = createMockStudent();
        User anotherMockUser = createMockStudent();
        anotherMockUser.setId(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getStudentEnrolledLectures(mockUser, anotherMockUser.getId()));
    }

    @Test
    public void getStudentCompletedCourses_should_callRepository() {
        User mockUser = createMockAdmin();

        Mockito.when(mockRepository.getStudentCompletedCourses(mockUser.getId()))
                .thenReturn(new ArrayList<>());

        service.getStudentCompletedCourses(mockUser.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getStudentCompletedCourses(mockUser.getId());
    }

    @Test
    public void getStudentCompletedCoursesWithGrades_should_callRepository() {
        User mockUser = createMockAdmin();

        Mockito.when(mockRepository.getStudentCompletedCoursesWithGrades(mockUser.getId()))
                .thenReturn(new HashMap<>());

        service.getStudentCompletedCoursesWithGrades(mockUser.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getStudentCompletedCoursesWithGrades(mockUser.getId());
    }

    @Test
    public void checkUserById_should_throw_when_userIsNotSameStudent() {
        User mockUser = createMockStudent();
        User anotherMockUser = createMockStudent();
        anotherMockUser.setId(2);

        Mockito.when(service.getUserById(mockUser.getId()))
                .thenReturn(mockUser);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.checkUserById(anotherMockUser, mockUser.getId()));
    }

    @Test
    public void checkUserById_should_throw_when_userIsNotSameTeacher() {
        User mockUser = createMockTeacher();
        User anotherMockUser = createMockTeacher();
        anotherMockUser.setId(2);

        Mockito.when(service.getUserById(mockUser.getId()))
                .thenReturn(mockUser);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.checkUserById(anotherMockUser, mockUser.getId()));
    }

    @Test
    public void checkUserById_should_throw_when_userIsNotAdmin() {
        User mockUser = createMockAdmin();
        User anotherMockUser = createMockStudent();
        anotherMockUser.setId(2);

        Mockito.when(service.getUserById(mockUser.getId()))
                .thenReturn(mockUser);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.checkUserById(anotherMockUser, mockUser.getId()));
    }

    @Test
    public void checkUserById_should_returnUser_when_matchExist() {
        User mockUser = createMockAdmin();

        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        User result = service.getUserById(mockUser.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getFirstName(), result.getFirstName()),
                () -> Assertions.assertEquals(mockUser.getLastName(), result.getLastName()),
                () -> Assertions.assertEquals(mockUser.getPassword(), result.getPassword()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail())
        );
    }

    @Test
    public void createUser_should_throw_when_user_WithSameEmailExists() {
        User mockUser = createMockAdmin();


        Mockito.when(mockRepository.getByField("email", mockUser.getEmail()))
                .thenReturn(mockUser);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.createUser(mockUser));
    }

    @Test
    public void createUser_should_callRepository_when_user_WithSameEmailDoesNotExists() {
        User mockUser = createMockAdmin();

        Mockito.when(mockRepository.getByField("email", mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        service.createUser(mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockUser);
    }

    @Test
    public void updateUser_should_callRepository_when_userNotIsAdminOrCreator() {
        User mockUser = createMockStudent();
        User anotherMockUser = createMockStudent();
        anotherMockUser.setId(2);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.updateUser(mockUser, anotherMockUser));
    }

    @Test
    public void updateUser_should_callRepository_when_userIsAdminOrCreator() {
        User mockUser = createMockAdmin();
        User anotherMockUser = createMockAdmin();

        Mockito.when(mockRepository.getByField("email", mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        service.updateUser(mockUser, anotherMockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    public void updateUser_should_throwException_when_UserEmailIsTaken() {
        User mockUser = createMockAdmin();
        User mockUpdator = createMockAdmin();
        mockUpdator.setId(2);

        Mockito.when(mockRepository.getByField("email", mockUser.getEmail()))
                .thenReturn(mockUpdator);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.updateUser(mockUser, mockUpdator));
    }

    //
    @Test
    public void updateUser_should_callRepository_when_tryingToUpdateExistingUser() {
        User mockUser = createMockAdmin();
        User anotherMockUser = createMockStudent();

        Mockito.when(mockRepository.getByField("email", mockUser.getEmail()))
                .thenReturn(mockUser);

        service.updateUser(mockUser, anotherMockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    void deleteUser_should_throwException_when_UserIsNotAdmin() {
        User mockUser = createMockStudent();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.deleteUser(mockUser.getId(), mockUser));
    }

    @Test
    void deleteUser_should_callRepository_when_initiatorIsAdmin() {
        User mockUser = createMockStudent();
        User mockUpdator = createMockAdmin();
        mockUpdator.setId(2);

        service.deleteUser(mockUser.getId(), mockUpdator);

        Mockito.verify(mockRepository, Mockito.times(1)).
                delete(mockUser.getId());
    }

    @Test
    public void getByEmail_should_returnUser_when_matchExist() {
        User mockUser = createMockAdmin();

        Mockito.when(mockRepository.getByField("email", mockUser.getEmail()))
                .thenReturn(mockUser);

        User result = service.getByEmail(mockUser.getEmail());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getFirstName(), result.getFirstName()),
                () -> Assertions.assertEquals(mockUser.getLastName(), result.getLastName()),
                () -> Assertions.assertEquals(mockUser.getPassword(), result.getPassword()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail())
        );
    }

    @Test
    public void updateUserRole_throw_when_User_Is_Not_Admin() {
        User mockUser = createMockStudent();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getUsers(mockUser));
    }

    @Test
    public void updateUserRole_should_callRepository_when_Update_To_Teacher() {
        User mockUser = createMockAdmin();
        User mockUpdator = createMockStudent();
        UserRole mockRole = createMockRole("Teacher");
        mockRole.setId(3);
        mockUpdator.setRole(mockRole);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUser);

        service.updateUserRole(1, mockUser, mockRole);

        Mockito.verify(mockRepository, Mockito.times(1))
                .updateUserRole(mockUpdator, mockRole);
    }

    @Test
    public void updateUserRole_should_callRepository_when_Update_To_Admin() {
        User mockUser = createMockAdmin();
        User mockUpdator = createMockStudent();
        UserRole mockRole = createMockRole("Admin");
        mockUpdator.setRole(mockRole);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUser);

        service.updateUserRole(1, mockUser, mockRole);

        Mockito.verify(mockRepository, Mockito.times(1))
                .updateUserRole(mockUpdator, mockRole);
    }

    @Test
    public void enrollOnCourse_throw_when_Date_Is_Before_ErolDate() {
        User user = createMockStudent();
        Course course = createMockCourse();
        LocalDate currentDate = LocalDate.now();
        LocalDate enrollDate = currentDate.plusDays(2);
        course.setStartDate(enrollDate);

        Mockito.when(mockCourseRepository.getById(course.getId()))
                .thenReturn(course);

        Assertions.assertTrue(currentDate.isBefore(enrollDate));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.enrollOnCourse(user, course.getId()));
    }

    @Test
    @MockitoSettings(strictness = Strictness.LENIENT)
    void enroll_should_callRepository() {

        User user = createMockStudent();
        Course course = createMockCourse();
        Mockito.when(mockRepository.getById(user.getId()))
                .thenReturn(user);
        Mockito.when(mockCourseRepository.getById(course.getId()))
                .thenReturn(course);
        Set<Course> courses = new HashSet<>();
        user.setStudentEnrolledCourses(courses);

        service.enrollOnCourse(user, course.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .enrollOnCourse(user, course.getId());
    }

    @Test
    public void enrollOnCourse_should_throw_when_student_AlreadyEnrolledCourse() {
        User user = createMockStudent();
        Course course = createMockCourse();

        Mockito.when(mockRepository.getById(user.getId()))
                .thenReturn(user);
        Mockito.when(mockCourseRepository.getById(course.getId()))
                .thenReturn(course);
        Set<Course> courses = new HashSet<>();
        courses.add(course);
        user.setStudentEnrolledCourses(courses);

        service.enrollOnCourse(user, course.getId());

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.enrollOnCourse(user, course.getId()));
    }

    @Test
    public void updateResetPasswordToken_throw_when_User_Is_Null() {
        User mockUser = createMockAdmin();
        String email = "email@abv.bg";

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.updateResetPasswordToken(mockUser.getEmail(), email));
    }

    @Test
    public void getByResetPasswordToken_should_callRepository() {
        String token = Mockito.anyString();

        service.getByResetPasswordToken(token);

        Mockito.verify(mockRepository, Mockito.times(1))
                .findByResetPasswordToken(token);

    }

    @Test
    public void updatePassword_should_callRepository() {
        User mockUser = createMockAdmin();
        mockUser.setPassword(Mockito.anyString());

        service.updatePassword(mockUser, mockUser.getPassword());

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUser);
    }

}
