package com.example.team12.virtualteacher.services;

import com.example.team12.virtualteacher.exceptions.DuplicateEntityException;
import com.example.team12.virtualteacher.exceptions.EntityNotFoundException;
import com.example.team12.virtualteacher.exceptions.UnauthorizedOperationException;
import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.Lecture;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.models.UserRole;
import com.example.team12.virtualteacher.models.enums.CourseStatusType;
import com.example.team12.virtualteacher.repositories.contracts.LectureRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class LectureServiceImplTests {

    @Mock
    LectureRepository mockRepository;

    @InjectMocks
    LectureServiceImpl lectureService;

    @Test
    public void getLectures_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        lectureService.getLectures();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getLecture_ById_should_returnUser_when_matchExist() {
        Lecture mockLecture = createMockLecture();

        Mockito.when(lectureService.getById(mockLecture.getId()))
                .thenReturn(mockLecture);

        Lecture result = lectureService.getById(mockLecture.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockLecture.getTitle(), result.getTitle()),
                () -> Assertions.assertEquals(mockLecture.getDescription(), result.getDescription()),
                () -> Assertions.assertEquals(mockLecture.getId(), result.getId())
        );
    }

    @Test
    public void create_should_throw_when_lectureWithSameTitleExists() {
        Lecture mockLecture = createMockLecture();

        Mockito.when(lectureService.getByTitle(mockLecture.getTitle()))
                .thenReturn(mockLecture);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> lectureService.createLecture(mockLecture));
    }

    @Test
    public void create_should_callRepository_when_lectureWithSameTitleDoesNotExist() {
        Lecture anotherMockLecture = createMockLecture();

        Mockito.when(mockRepository.getByField("title", anotherMockLecture.getTitle()))
                .thenThrow(new EntityNotFoundException("Lecture", "title", anotherMockLecture.getTitle() ));

        lectureService.createLecture(anotherMockLecture);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(Mockito.any(Lecture.class));
    }

    @Test
    public void update_should_throwException_when_userIsNotCreatorOrAdmin() {
        Lecture mockLecture = createMockLecture();
        User mockCreator = createMockUser();
        mockCreator.getRole().setName("Student");

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> lectureService.updateLecture(mockLecture, mockCreator));
    }

    private Lecture createMockLecture() {
        Lecture mockLecture = new Lecture();
        mockLecture.setTitle("testTitle");
        mockLecture.setDescription("testDescription");
        mockLecture.setCourse(createMockCourse());
      //  mockLecture.setId(1);

        return mockLecture;
    }

    public static Course createMockCourse() {
        Course mockCourse = new Course();
        mockCourse.setTitle("testCourse");
        mockCourse.setDescription("testCourse");
        mockCourse.setTopic("testCourse");
        mockCourse.setStartDate(LocalDate.now());
        mockCourse.setId(1);
        mockCourse.setStatus(CourseStatusType.PUBLISHED);

        return mockCourse;
    }

    public static User createMockUser() {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setFirstName("testFirstName");
        mockUser.setLastName("testLastName");
        mockUser.setEmail("testUsername@abv.bg");
        mockUser.setPassword("testPassword");
        mockUser.setEmail("pesho@abv.bg");
        mockUser.setRole((createMockRole("Teacher")));

        return mockUser;
    }

    public static UserRole createMockRole(String role) {
        var mockRole = new UserRole();
        mockRole.setId(1);
        mockRole.setName(role);
        return mockRole;
    }


}
