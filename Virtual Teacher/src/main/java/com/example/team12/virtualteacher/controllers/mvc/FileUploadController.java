package com.example.team12.virtualteacher.controllers.mvc;

import com.example.team12.virtualteacher.controllers.AuthenticationHelper;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.services.contracts.CourseService;
import com.example.team12.virtualteacher.services.contracts.LectureService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
public class FileUploadController {
    public static String uploadDirectoryForTeachers = System.getProperty("user.dir") + "/uploads" + "/teachers";
    public static String uploadDirectoryForStudents = System.getProperty("user.dir") + "/uploads" + "/students";

    public static String uploadDirectoryForProfilePictures = System.getProperty("user.dir") + "/src/main/resources" + "/static" + "/assets" + "/profile-pictures";

    public static String uploadDirectoryForCoursePictures = System.getProperty("user.dir") + "/src/main/resources" + "/static" + "/assets" + "/course-pictures";

    private static final String EXTERNAL_FILE_PATH = uploadDirectoryForTeachers;

    private LectureService lectureService;
    private AuthenticationHelper authenticationHelper;
    private final CourseService courseService;

    public FileUploadController(LectureService lectureService,
                                AuthenticationHelper authenticationHelper,
                                CourseService courseService) {
        this.lectureService = lectureService;
        this.authenticationHelper = authenticationHelper;
        this.courseService = courseService;
    }

    //TODO To extract the 2 upload methods in 1 and reuse the duplicate code
    @RequestMapping("/upload")
    public String upload(Model model, @RequestParam("lectureId") int lectureId, @RequestParam("files") MultipartFile[] files,
                         HttpSession httpSession) {

        User user = authenticationHelper.tryGetUser(httpSession);

        String lectureTitle = lectureService.getById(lectureId).getTitle();
        StringBuilder fileNames = new StringBuilder();
        for (MultipartFile file : files) {
            String fileContentType = file.getContentType();
            String fileExtension = "txt";

            Path fileNameAndPath;

            if (user.checkIfUserStudent()) {
                fileNameAndPath = Paths.get(uploadDirectoryForStudents, String.format("%s-%s.%s", lectureTitle, user.getEmail(), fileExtension));
            } else {
                fileNameAndPath = Paths.get(uploadDirectoryForTeachers, String.format("%s.%s", lectureTitle, fileExtension));
            }
            fileNames.append(file.getOriginalFilename() + " ");
            try {
                Files.write(fileNameAndPath, file.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        model.addAttribute("msg", "Successfully uploaded files " + fileNames.toString());
        return "uploadstatus-view";
    }

    @RequestMapping("/uploadProfilePicture")
    public String uploadProfilePicture(Model model,
                                       @RequestParam("files") MultipartFile[] files,
                                       HttpSession httpSession) {

        User user = authenticationHelper.tryGetUser(httpSession);
        StringBuilder fileNames = new StringBuilder();
        for (MultipartFile file : files) {
            String fileExtension = "jpg";
            Path fileNameAndPath;
            fileNameAndPath = Paths.get(uploadDirectoryForProfilePictures, String.format("%s.%s", user.getEmail(), fileExtension));
            fileNames.append(file.getOriginalFilename() + " ");
            try {
                Files.write(fileNameAndPath, file.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        model.addAttribute("msg", "Successfully uploaded files " + fileNames.toString());
        return "uploadstatus-view";
    }

    @GetMapping("download/file/{fileName:.+}")
    public String downloadPDFResource(HttpServletRequest request, HttpServletResponse response,
                                      @PathVariable("fileName") String fileName, HttpSession httpSession) throws IOException {

        User user = authenticationHelper.tryGetUser(httpSession);

        File file = new File(uploadDirectoryForTeachers);
        if (user.checkIfUserStudent()) {
            file = new File(uploadDirectoryForTeachers + "/" + fileName + ".txt");
        }

        if (file.exists()) {

            //get the mimetype
            String mimeType = URLConnection.guessContentTypeFromName(file.getName());
            if (mimeType == null) {
                //unknown mimetype so set the mimetype to application/octet-stream
                mimeType = "application/octet-stream";
            }

            response.setContentType(mimeType);

            response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));

            //Here we have mentioned it to show as attachment
            response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + file.getName() + "\""));

            response.setContentLength((int) file.length());

            InputStream inputStream = new BufferedInputStream(new FileInputStream(file));

            FileCopyUtils.copy(inputStream, response.getOutputStream());

            return "redirect:/download-success";
        } else {
            return "not-found";
        }
    }

    @RequestMapping("/uploadCoursePicture")
    public String uploadCoursePicture(Model model, @RequestParam("courseId") int courseId, @RequestParam("files") MultipartFile[] files,
                                      HttpSession httpSession) {

        User user = authenticationHelper.tryGetUser(httpSession);

        String courseTitle = courseService.getById(courseId).getTitle();
        StringBuilder fileNames = new StringBuilder();
        for (MultipartFile file : files) {
            String fileContentType = file.getContentType();
            String fileExtension = "jpg";

            Path fileNameAndPath;


            fileNameAndPath = Paths.get(uploadDirectoryForCoursePictures, String.format("%s.%s", courseTitle, fileExtension));

            fileNames.append(file.getOriginalFilename() + " ");
            try {
                Files.write(fileNameAndPath, file.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        model.addAttribute("msg", "Successfully uploaded files " + fileNames.toString());
        return "uploadstatus-view";
    }
}
