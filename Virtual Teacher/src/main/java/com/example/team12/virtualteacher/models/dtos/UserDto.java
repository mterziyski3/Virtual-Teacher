package com.example.team12.virtualteacher.models.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserDto {

    private int id;

    @NotNull
    @Size(min = 2, max = 20, message = "First name's length should be between 2 and 20 symbols.")
    private String firstName;
    @NotNull
    @Size(min = 2, max = 20, message = "Last name's length should be between 2 and 20 symbols.")
    private String lastName;
    @NotNull
    @Email
    private String email;

    private String password;

    @NotNull
    private String roleName;


    public UserDto() {

    }

    public UserDto(int id, String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
