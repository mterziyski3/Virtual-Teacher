package com.example.team12.virtualteacher.models;

import java.util.Optional;

//The anonymous users must also be able to browse the catalog of available courses.
// The user must be able to filter the catalog by
// course name,
// course topic,
// teacher,?!
// and rating,
// as well as to sort the catalog by name and/or rating.
public class FilterOptionsCourse {

    private Optional<String> title;
    private Optional<String> topic;
    private Optional<String> creator;
    private Optional<Double> rating;
    private Optional<String> sortBy;
    private Optional<String> sortOrder;

    public FilterOptionsCourse(Optional<String> title,
                               Optional<String> topic,
                               Optional<String> creator,
                               Optional<Double> rating,
                               Optional<String> sortBy,
                               Optional<String> sortOrder
    ) {
        this.title = title;
        this.topic = topic;
        this.creator = creator;
        this.rating = rating;
        this.sortBy = sortBy;
        this.sortOrder = sortOrder;
    }

    public Optional<String> getTitle() {
        return title;
    }

    public Optional<String> getTopic() {
        return topic;
    }

    public Optional<String> getCreator() {
        return creator;
    }

    public Optional<Double> getRating() {
        return rating;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }
}
