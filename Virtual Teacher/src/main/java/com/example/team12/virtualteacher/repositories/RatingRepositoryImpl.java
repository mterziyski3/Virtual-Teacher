package com.example.team12.virtualteacher.repositories;

import com.example.team12.virtualteacher.exceptions.EntityNotFoundException;
import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.Rating;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.repositories.contracts.RatingRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Repository
public class RatingRepositoryImpl extends AbstractCRUDRepository<Rating> implements RatingRepository {

    @Autowired
    protected RatingRepositoryImpl(SessionFactory sessionFactory) {

        super(Rating.class, sessionFactory);
    }

    @Override
    public Rating rate(User user, Course course) {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from Rating" + " where user = :user and" +
                    " course = :course", Rating.class);

            query.setParameter("user", user);
            query.setParameter("course", course);

            List<Rating> result = query.list();

            if (result.size() == 0) {
                throw new EntityNotFoundException(
                        String.format("Rating with %s and course with %s not found",
                                user.getEmail(), course.getTitle()));
            }
            return result.get(0);
        }
    }

    @Override
    public Double getAvgRating(Course course) {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("select avg(rate)"
                    + "from Rating where course = :course", Double.class);

            query.setParameter("course", course);

            Double result = query.uniqueResult();
            return BigDecimal.valueOf(result).setScale(1, RoundingMode.HALF_EVEN).doubleValue();
        }
    }

    public boolean ifStudentRatedCourse(User user, Course course) {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery(
                    "from Rating where course = :course and user = :user", Rating.class);

            query.setParameter("course", course);
            query.setParameter("user", user);
            return query.uniqueResult() == null;
        }
    }
}
