package com.example.team12.virtualteacher.controllers.rest;

import com.example.team12.virtualteacher.controllers.AuthenticationHelper;
import com.example.team12.virtualteacher.exceptions.DuplicateEntityException;
import com.example.team12.virtualteacher.exceptions.EntityNotFoundException;
import com.example.team12.virtualteacher.exceptions.UnauthorizedOperationException;
import com.example.team12.virtualteacher.mappers.CourseMapper;
import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.FilterOptionsCourse;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.models.dtos.CourseDto;
import com.example.team12.virtualteacher.models.dtos.RatingDto;
import com.example.team12.virtualteacher.services.contracts.CourseService;
import com.example.team12.virtualteacher.services.contracts.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/courses")
public class CourseController {

    private final CourseService courseService;

    private final CourseMapper courseMapper;

    private final RatingService ratingService;

    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CourseController(CourseService courseService, CourseMapper courseMapper, RatingService ratingService, AuthenticationHelper authenticationHelper) {
        this.courseService = courseService;
        this.courseMapper = courseMapper;
        this.ratingService = ratingService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/filter")
    public List<Course> filter(
            @RequestParam(required = false) Optional<String> title,
            @RequestParam(required = false) Optional<String> topic,
            @RequestParam(required = false) Optional<String> creator,
            @RequestParam(required = false) Optional<Double> rating,
            @RequestParam(required = false) Optional<String> sortBy,
            @RequestParam(required = false) Optional<String> sortOrder
    ) {
        return courseService.filter(new FilterOptionsCourse(title, topic, creator,
                rating, sortBy, sortOrder));
    }

    @GetMapping
    public List<Course> getAllCourses() {
        return courseService.getAll();
    }

    @GetMapping("/{id}")
    public Course getById(@PathVariable int id) {
        try {
            return courseService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Course create(@RequestHeader HttpHeaders headers, @Valid @RequestBody CourseDto courseDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Course course = courseMapper.dtoToObject(user, courseDto, new Course());
            user.setId(user.getId());
            courseService.create(course, user);
            return course;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}/rate")
    public void rate(@RequestHeader HttpHeaders headers,
                     @PathVariable int id,
                     @RequestBody RatingDto ratingDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Course courseToRate = courseService.getById(id);
            ratingService.rate(courseToRate, user, ratingDto.getRating());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Course update(@RequestHeader HttpHeaders headers, @PathVariable int id,
                         @Valid @RequestBody CourseDto courseDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Course courseToUpdate = courseService.getById(id);
            Course course = courseMapper.dtoToObject(user, courseDto, courseToUpdate);
            courseService.update(course, user);
            return course;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            courseService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
