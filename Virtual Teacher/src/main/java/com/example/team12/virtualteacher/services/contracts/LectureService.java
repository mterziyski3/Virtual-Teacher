package com.example.team12.virtualteacher.services.contracts;


import com.example.team12.virtualteacher.models.Assignment;
import com.example.team12.virtualteacher.models.FilterOptionsLecture;
import com.example.team12.virtualteacher.models.Lecture;
import com.example.team12.virtualteacher.models.User;

import java.util.List;

public interface LectureService {
    List<Lecture> getLectures();

    Lecture getLecture(int id);

    Lecture createLecture(Lecture lecture);

    Lecture updateLecture(Lecture lecture, User userWhoIsUpdating);

    void deleteLecture(int id, User UserWhoIsUpdating);

    List<Lecture> filter(FilterOptionsLecture filterOptions);

    void assessAssignment(Assignment assignment);

    Lecture getByTitle(String title);

    Lecture getById(int id);
}
