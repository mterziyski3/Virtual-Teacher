package com.example.team12.virtualteacher.repositories.contracts;

import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.FilterOptionsCourse;

import java.util.List;

public interface CourseRepository extends BaseCRUDRepository<Course> {

    List<Course> filter(FilterOptionsCourse filterOptionsCourse);

    List<Course> filterToPublished(FilterOptionsCourse filterOptionsCourse);

    List<Course> getPublished();

}
