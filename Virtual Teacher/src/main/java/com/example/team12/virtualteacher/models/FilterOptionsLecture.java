package com.example.team12.virtualteacher.models;

import java.util.Optional;

public class FilterOptionsLecture {
    private Optional<String> title;
    private Optional<String> description;
    private Optional<String> sortBy;
    private Optional<String> sortOrder;

    public FilterOptionsLecture(Optional<String> title, Optional<String> description,
                                Optional<String> sortBy, Optional<String> sortOrder) {
        this.title = title;
        this.description = description;
        this.sortBy = sortBy;
        this.sortOrder = sortOrder;
    }

    public Optional<String> getTitle() {
        return title;
    }

    public void setTitle(Optional<String> title) {
        this.title = title;
    }

    public Optional<String> getDescription() {
        return description;
    }

    public void setDescription(Optional<String> description) {
        this.description = description;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public void setSortBy(Optional<String> sortBy) {
        this.sortBy = sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Optional<String> sortOrder) {
        this.sortOrder = sortOrder;
    }
}
