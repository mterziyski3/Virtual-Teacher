package com.example.team12.virtualteacher.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;
    @NotNull
    @Column(name = "first_name")
    private String firstName;
    @NotNull
    @Column(name = "last_name")
    private String lastName;
    @Email
    @NotNull
    @Column(name = "email_address")
    private String email;
    @NotNull
    @JsonIgnore
    @Column(name = "password")
    private String password;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private UserRole role;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "requested_role_id")
    private UserRole requestedUserRole;

    @Formula(value = " concat(first_name, ' ', last_name)")
    private String fullName;

    @Column(name = "verification_code", length = 64)
    private String verificationCode;

    private boolean enabled = false;

    @Column(name = "reset_password_token")
    private String resetPasswordToken;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "students_lectures", joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "lecture_id"))
    private Set<Lecture> studentEnrolledLectures;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "students_courses", joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id"))
    private Set<Course> studentEnrolledCourses;


    public User() {

    }

    public User(int id, String firstName, String lastName, String email, String password) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public boolean checkIfUserAdmin() {

        return role.getName().equalsIgnoreCase("Admin");
    }

    public boolean checkIfUserStudent() {
        return role.getName().equalsIgnoreCase("Student");
    }

    public boolean checkIfUserTeacher() {
        return role.getName().equalsIgnoreCase("Teacher");
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public UserRole getRequestedUserRole() {
        return requestedUserRole;
    }

    public void setRequestedUserRole(UserRole requestedUserRole) {
        this.requestedUserRole = requestedUserRole;
    }


    public Set<Lecture> getStudentEnrolledLectures() {
        return studentEnrolledLectures;
    }

    public void setStudentEnrolledLectures(Set<Lecture> studentEnrolledLectures) {
        this.studentEnrolledLectures = studentEnrolledLectures;
    }

    public Set<Course> getStudentEnrolledCourses() {
        return studentEnrolledCourses;
    }


    public void setStudentEnrolledCourses(Set<Course> studentEnrolledCourses) {
        this.studentEnrolledCourses = studentEnrolledCourses;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getResetPasswordToken() {
        return resetPasswordToken;
    }

    public void setResetPasswordToken(String resetPasswordToken) {
        this.resetPasswordToken = resetPasswordToken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return id == user.id && Objects.equals(firstName, user.firstName)
                && Objects.equals(lastName, user.lastName)
                && Objects.equals(email, user.email)
                && Objects.equals(password, user.password)
                && Objects.equals(role, user.role)
                && Objects.equals(studentEnrolledCourses, user.studentEnrolledCourses);

    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }
}

