package com.example.team12.virtualteacher.models.enums;

public enum CourseStatusType {
    DRAFT, PUBLISHED;

    @Override
    public String toString() {
        switch (this) {
            case DRAFT:
                return "Draft";
            case PUBLISHED:
                return "Published";
            default:
                throw new IllegalArgumentException();
        }
    }
}
