package com.example.team12.virtualteacher.models.dtos;

import java.time.LocalDate;
import java.util.Set;

public class CourseDtoOut {

    private int id;

    private String title;

    private String topic;

    private String description;

    private LocalDate startDate;

    private String creator;

    private Set<LectureDtoOutForCourse> lectures;

    public CourseDtoOut() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Set<LectureDtoOutForCourse> getLectures() {
        return lectures;
    }

    public void setLectures(Set<LectureDtoOutForCourse> lectures) {
        this.lectures = lectures;
    }
}
