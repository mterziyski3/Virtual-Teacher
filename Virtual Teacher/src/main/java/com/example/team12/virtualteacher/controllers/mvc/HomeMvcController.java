package com.example.team12.virtualteacher.controllers.mvc;

import com.example.team12.virtualteacher.controllers.AuthenticationHelper;
import com.example.team12.virtualteacher.exceptions.AuthenticationFailureException;
import com.example.team12.virtualteacher.mappers.CourseMapper;
import com.example.team12.virtualteacher.models.Comment;
import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.FilterOptionsCourse;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.models.dtos.CourseFilterDto;
import com.example.team12.virtualteacher.services.contracts.CommentService;
import com.example.team12.virtualteacher.services.contracts.CourseService;
import com.example.team12.virtualteacher.services.contracts.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final CourseService courseService;
    private final UserService userService;
    private final CourseMapper courseMapper;

    private final CommentService commentService;
    private final AuthenticationHelper authenticationHelper;

    public HomeMvcController(CourseService courseService, UserService userService, CourseMapper courseMapper, CommentService commentService, AuthenticationHelper authenticationHelper) {
        this.courseService = courseService;
        this.userService = userService;
        this.courseMapper = courseMapper;
        this.commentService = commentService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession httpSession) {
        return httpSession.getAttribute("currentUser") != null;
    }

    @ModelAttribute("loggedUser")
    public User populateLoggedUser(HttpSession httpSession) {
        try {
            return authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return new User();
        }
    }

    @GetMapping
    public String showHomePage(@ModelAttribute("filterOptions") CourseFilterDto courseFilterDto,
                               Model model,
                               HttpSession session
    ) {
        if (session.getAttribute("currentUser") != null) {
            String email = session.getAttribute("currentUser").toString();
            User user = userService.getByEmail(email);
            model.addAttribute("user", user);
            model.addAttribute("courses", courseService.getAll());
        }
        FilterOptionsCourse filterOptionsCourse = courseMapper.fromFilterDto(courseFilterDto);
        List<Course> courses = courseService.filterToPublished(filterOptionsCourse);
        List<Comment> comments = commentService.getAll();
        model.addAttribute("filterOptions", courseFilterDto);
        model.addAttribute("courses", courses);
        model.addAttribute("comments", comments);
        return "index";
    }
}
