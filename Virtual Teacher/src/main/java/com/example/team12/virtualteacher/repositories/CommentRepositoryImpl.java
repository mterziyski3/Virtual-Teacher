package com.example.team12.virtualteacher.repositories;

import com.example.team12.virtualteacher.models.Comment;
import com.example.team12.virtualteacher.repositories.contracts.CommentRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CommentRepositoryImpl extends AbstractCRUDRepository<Comment> implements CommentRepository {

    @Autowired
    public CommentRepositoryImpl(SessionFactory sessionFactory) {
        super(Comment.class, sessionFactory);
    }
}
