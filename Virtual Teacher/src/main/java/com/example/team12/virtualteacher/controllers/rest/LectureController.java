package com.example.team12.virtualteacher.controllers.rest;

import com.example.team12.virtualteacher.controllers.AuthenticationHelper;
import com.example.team12.virtualteacher.exceptions.DuplicateEntityException;
import com.example.team12.virtualteacher.exceptions.EntityNotFoundException;
import com.example.team12.virtualteacher.exceptions.UnauthorizedOperationException;
import com.example.team12.virtualteacher.mappers.LectureMapper;
import com.example.team12.virtualteacher.models.FilterOptionsLecture;
import com.example.team12.virtualteacher.models.Lecture;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.models.dtos.LectureDto;
import com.example.team12.virtualteacher.services.contracts.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/lectures")
public class LectureController {

    private final LectureService service;
    private final LectureMapper lectureMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public LectureController(LectureService service, LectureMapper lectureMapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.lectureMapper = lectureMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/filter")
    public List<Lecture> filter(
            @RequestParam(required = false) Optional<String> title,
            @RequestParam(required = false) Optional<String> description,
            @RequestParam(required = false) Optional<String> sortBy,
            @RequestParam(required = false) Optional<String> sortOrder) {
        return service.filter(new FilterOptionsLecture(title, description,
                sortBy, sortOrder));
    }

    @GetMapping
    public List<Lecture> getLectures() {
        return service.getLectures();
    }

    @GetMapping("/{id}")
    public Lecture getLecture(@PathVariable @Valid int id) {
        try {
            return service.getLecture(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Lecture createLecture(@RequestHeader HttpHeaders headers, @Valid @RequestBody LectureDto lectureDto) {
        try {
            Lecture lecture = lectureMapper.fromDto(lectureDto, new Lecture());
            service.createLecture(lecture);
            return lecture;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Lecture updateLecture(@RequestHeader HttpHeaders headers, @PathVariable int id,
                                 @RequestBody @Valid LectureDto lectureDto) {
        try {
            User userWhoIsUpdating = authenticationHelper.tryGetUser(headers);
            Lecture lectureToUpdate = service.getLecture(id);

            Lecture lecture = lectureMapper.fromDto(lectureDto, lectureToUpdate);
            service.updateLecture(lecture, userWhoIsUpdating);

            return lecture;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

    }

    @DeleteMapping("/{id}")
    public void deleteLecture(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User userWhoIsUpdating = authenticationHelper.tryGetUser(headers);
            service.deleteLecture(id, userWhoIsUpdating);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());

        }
    }
}
