package com.example.team12.virtualteacher.services.contracts;

import com.example.team12.virtualteacher.models.Comment;
import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.User;

import java.util.List;

public interface CommentService {

    List<Comment> getAll();

    Comment getById(int id);

    Comment create(Comment comment, Course course, User user);

    Comment update(Comment comment, User user);

    void delete(int id, User user);
}
