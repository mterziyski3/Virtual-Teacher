package com.example.team12.virtualteacher.services;

import com.example.team12.virtualteacher.exceptions.DuplicateEntityException;
import com.example.team12.virtualteacher.exceptions.EntityNotFoundException;
import com.example.team12.virtualteacher.exceptions.UnauthorizedOperationException;
import com.example.team12.virtualteacher.models.Comment;
import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.repositories.contracts.CommentRepository;
import com.example.team12.virtualteacher.services.contracts.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    private static final String CREATE_COMMENT_ERROR_MESSAGE = "Only student can create a comment.";
    private static final String MODIFY_COMMENT_ERROR_MESSAGE = "Only admin or author can modify a comment.";
    private static final String DELETE_COMMENT_ERROR_MESSAGE = "Only admin or author can delete a comment.";
    private final CommentRepository commentRepository;

    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Override
    public List<Comment> getAll() {
        return commentRepository.getAll();
    }

    @Override
    public Comment getById(int id) {
        return commentRepository.getById(id);
    }

    @Override
    public Comment create(Comment comment, Course course, User user) {
        if (user.checkIfUserAdmin() && user.checkIfUserTeacher()) {
            throw new UnauthorizedOperationException(CREATE_COMMENT_ERROR_MESSAGE);
        }
        boolean duplicateExists = true;
        try {
            commentRepository.getByField("content", comment.getContent());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Comment", "name", comment.getContent());
        }
        comment.setCreator(user);
        commentRepository.create(comment);
        return comment;
    }

    @Override
    public Comment update(Comment comment, User user) {
        if (!user.checkIfUserAdmin() && !comment.getCreator().equals(user)) {
            throw new UnauthorizedOperationException(MODIFY_COMMENT_ERROR_MESSAGE);
        }
        boolean duplicateExists = true;
        try {
            Comment existingComment = commentRepository.getByField("content", comment.getContent());
            if (existingComment.getId() == comment.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Comment", "name", comment.getContent());
        }
        commentRepository.update(comment);
        return comment;
    }

    @Override
    public void delete(int id, User user) {
        Comment comment = commentRepository.getById(id);
        if (!user.checkIfUserAdmin() && !comment.getCreator().equals(user)) {
            throw new UnauthorizedOperationException(DELETE_COMMENT_ERROR_MESSAGE);
        }
    }
}

