package com.example.team12.virtualteacher.controllers.rest;

import com.example.team12.virtualteacher.controllers.AuthenticationHelper;
import com.example.team12.virtualteacher.exceptions.DuplicateEntityException;
import com.example.team12.virtualteacher.exceptions.EntityNotFoundException;
import com.example.team12.virtualteacher.exceptions.UnauthorizedOperationException;
import com.example.team12.virtualteacher.mappers.CourseMapper;
import com.example.team12.virtualteacher.mappers.UserMapper;
import com.example.team12.virtualteacher.models.*;
import com.example.team12.virtualteacher.models.dtos.CourseEnrolDto;
import com.example.team12.virtualteacher.models.dtos.UserDto;
import com.example.team12.virtualteacher.services.contracts.AssignmentService;
import com.example.team12.virtualteacher.services.contracts.CourseService;
import com.example.team12.virtualteacher.services.contracts.LectureService;
import com.example.team12.virtualteacher.services.contracts.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;
    private final CourseMapper courseMapper;
    private final CourseService courseService;
    private final AssignmentService assignmentService;

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    public UserController(UserService service,
                          UserMapper userMapper,
                          AuthenticationHelper authenticationHelper,
                          CourseMapper courseMapper,
                          CourseService courseService,
                          AssignmentService assignmentService) {
        this.userService = service;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
        this.courseMapper = courseMapper;
        this.courseService = courseService;
        this.assignmentService = assignmentService;
    }

    @GetMapping("/filter")
    public List<User> filter(@RequestHeader HttpHeaders headers,
                             @RequestParam(required = false) Optional<String> firstName,
                             @RequestParam(required = false) Optional<String> lastName,
                             @RequestParam(required = false) Optional<String> email,
                             @RequestParam(required = false) Optional<String> sortBy,
                             @RequestParam(required = false) Optional<String> sortOrder) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return userService.filter(new FilterOptionsUser(firstName, lastName, email,
                    sortBy, sortOrder), user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping
    public List<User> getUsers(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return userService.getUsers(user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/students")
    public List<User> getStudents(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return userService.getAllStudents(user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/courses/{id}")
    public List<Course> getTeacherCreatedCourses(@RequestHeader HttpHeaders headers,
                                                 @PathVariable @Valid int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return userService.getTeacherCreatedCourses(user, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @GetMapping("/student-completedCourses/{id}")
    public Map<String, Double> getStudentCompletedCourses(@RequestHeader HttpHeaders headers,
                                                          @PathVariable @Valid int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return userService.getStudentCompletedCoursesWithGrades(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @GetMapping("/course-grade/{id}")
    public Double getStudentAvgCourseGrade(@RequestHeader HttpHeaders headers,
                                           @PathVariable @Valid int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return assignmentService.getStudentAvgCourseGrade(user.getId(), id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/hasPassed/{courseId}")
    public boolean studentHasPassedCourse(@RequestHeader HttpHeaders headers, @PathVariable int courseId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Course course = courseService.getById(courseId);
            return assignmentService.studentHasPassedCourse(user, course);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/studentsGrades/{courseId}")
    public List<Integer> getStudentGrades(@RequestHeader HttpHeaders headers, @PathVariable int courseId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Course course = courseService.getById(courseId);
            return assignmentService.getStudentGrades(user, course);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/studentAssignments/{courseId}")
    public List<Assignment> getStudentAssignment(@RequestHeader HttpHeaders headers,
                                                 @PathVariable int courseId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Course course = courseService.getById(courseId);
            return assignmentService.getStudentAssignment(user, course);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/student-lectures/{id}")
    public List<Lecture> getStudentEnrolledLectures(@RequestHeader HttpHeaders headers,
                                                    @PathVariable @Valid int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return userService.getStudentEnrolledLectures(user, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public User checkUserById(@RequestHeader HttpHeaders headers,
                              @PathVariable @Valid int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return userService.checkUserById(user, id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User updateUser(@RequestHeader HttpHeaders headers,
                           @PathVariable int id, @RequestBody @Valid UserDto userDto) {
        try {
            User userWhoIsUpdating = authenticationHelper.tryGetUser(headers);
            User user = userMapper.fromDto(userDto, id);
            userService.updateUser(user, userWhoIsUpdating);
            return user;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @PutMapping("/enrol-course")
    public void enrollOnCourse(@RequestHeader HttpHeaders headers,
                               @Valid @RequestBody CourseEnrolDto courseEnrolDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Course course = courseMapper.fromCourseEnrolDto(courseEnrolDto);
            userService.enrollOnCourse(user, course.getId());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
