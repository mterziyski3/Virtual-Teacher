package com.example.team12.virtualteacher.models.dtos;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;

public class UserRoleDto {
    @NotEmpty
    @Column(name = "name")
    private String name;

    public UserRoleDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
