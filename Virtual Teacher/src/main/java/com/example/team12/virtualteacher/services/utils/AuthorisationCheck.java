package com.example.team12.virtualteacher.services.utils;

import com.example.team12.virtualteacher.exceptions.UnauthorizedOperationException;
import com.example.team12.virtualteacher.models.User;

import static com.example.team12.virtualteacher.services.utils.Constants.*;

public class AuthorisationCheck {

    public static void checkIfCanAccessAdminInfo(User user)
            throws UnauthorizedOperationException {
        if (!user.checkIfUserAdmin()) {
            throw new UnauthorizedOperationException(ADMIN_INFO_ERROR_MESSAGE);
        }
    }

    public static void checkIfCanAccessAdminInfo(User user, int teacherId)
            throws UnauthorizedOperationException {
        if (!user.checkIfUserAdmin() && user.getId() != teacherId) {
            throw new UnauthorizedOperationException(ACCESS_ADMIN_INFO_ERROR_MESSAGE);
        }
    }

    public static void checkIfCanAccessTeacherInfo(User user)
            throws UnauthorizedOperationException {
        if (!user.checkIfUserAdmin() && !user.checkIfUserTeacher()) {
            throw new UnauthorizedOperationException(ACCESS_TEACHER_INFO_ERROR_MESSAGE);
        }
    }

    public static void checkIfCanAccessStudentInfo(User user, int studentId)
            throws UnauthorizedOperationException {
        if (!user.checkIfUserAdmin() && !user.checkIfUserTeacher() && user.getId() != studentId) {
            throw new UnauthorizedOperationException(ACCESS_STUDENT_INFO_ERROR_MESSAGE);
        }
    }
}
