package com.example.team12.virtualteacher.models.dtos;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class RatingDto {

    private double rating;

    @Min(1)
    @Max(5)
    public RatingDto() {
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
