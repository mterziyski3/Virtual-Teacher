package com.example.team12.virtualteacher.repositories.contracts;

import com.example.team12.virtualteacher.models.Comment;

public interface CommentRepository extends BaseCRUDRepository<Comment> {

}
