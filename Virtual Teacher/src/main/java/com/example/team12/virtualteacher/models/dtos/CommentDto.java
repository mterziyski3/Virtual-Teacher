package com.example.team12.virtualteacher.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CommentDto {

    private int id;

    @NotNull
    @Size(min = 75, max = 250, message = "Reply should be between 5 and 500 symbols.")
    private String content;


    public CommentDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
