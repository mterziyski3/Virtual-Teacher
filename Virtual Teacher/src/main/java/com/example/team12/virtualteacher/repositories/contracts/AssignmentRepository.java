package com.example.team12.virtualteacher.repositories.contracts;

import com.example.team12.virtualteacher.models.Assignment;
import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.User;

import java.util.List;

public interface AssignmentRepository extends BaseCRUDRepository<Assignment> {

    double getStudentAvgCourseGrade(User student, Course course);

    boolean studentHasPassedCourse(User user, Course course);

    List<Integer> getStudentGrades(User user, Course course);

    List<Assignment> getStudentAssignment(User user, Course course);
}
