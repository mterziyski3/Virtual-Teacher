package com.example.team12.virtualteacher.services;

import com.example.team12.virtualteacher.exceptions.UnauthorizedOperationException;
import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.Rating;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.repositories.contracts.AssignmentRepository;
import com.example.team12.virtualteacher.repositories.contracts.CourseRepository;
import com.example.team12.virtualteacher.repositories.contracts.RatingRepository;
import com.example.team12.virtualteacher.services.contracts.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RatingServiceImpl implements RatingService {

    public static final String ONLY_A_STUDENT_CAN_RATE_A_COURSE = "Only a student can rate a course";

    public static final String ONLY_A_STUDENT_WITH_AVG_MIN_GRADE_CAN_RATE_A_COURSE =
            "Only a student with a minimum grade point average of for the course can rate a course";
    public static final String YOU_ALREADY_RATE_THIS_COURSE = "You already rate this course";
    private final CourseRepository courseRepository;

    private final AssignmentRepository assignmentRepository;

    private final RatingRepository ratingRepository;

    @Autowired
    public RatingServiceImpl(CourseRepository courseRepository, AssignmentRepository assignmentRepository, RatingRepository ratingRepository) {
        this.courseRepository = courseRepository;
        this.assignmentRepository = assignmentRepository;
        this.ratingRepository = ratingRepository;
    }


    @Override
    public void rate(Course courseToRate, User user, double rating) {
        if (user.checkIfUserAdmin() || user.checkIfUserTeacher()) {
            throw new UnauthorizedOperationException(ONLY_A_STUDENT_CAN_RATE_A_COURSE);
        }
        double avgGrade = assignmentRepository.getStudentAvgCourseGrade(user, courseToRate);
        double coursePassingGrade = courseToRate.getPassingGrade();
        if (avgGrade < coursePassingGrade) {
            throw new UnauthorizedOperationException(ONLY_A_STUDENT_WITH_AVG_MIN_GRADE_CAN_RATE_A_COURSE);
        }

        if (!ifStudentRatedCourse(user, courseToRate)) {
            throw new UnauthorizedOperationException(YOU_ALREADY_RATE_THIS_COURSE);
        } else {
            Rating newRate = new Rating(rating, user, courseToRate);
            ratingRepository.create(newRate);
            courseToRate.setRating(getAvgRating(courseToRate));
            courseRepository.update(courseToRate);
        }
    }

    @Override
    public Double getAvgRating(Course course) {
        return ratingRepository.getAvgRating(course);
    }

    @Override
    public boolean ifStudentRatedCourse(User user, Course course) {
        return ratingRepository.ifStudentRatedCourse(user, course);
    }
}
