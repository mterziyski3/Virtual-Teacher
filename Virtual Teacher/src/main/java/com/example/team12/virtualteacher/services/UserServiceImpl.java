package com.example.team12.virtualteacher.services;

import com.example.team12.virtualteacher.exceptions.DuplicateEntityException;
import com.example.team12.virtualteacher.exceptions.EntityNotFoundException;
import com.example.team12.virtualteacher.exceptions.UnauthorizedOperationException;
import com.example.team12.virtualteacher.models.*;
import com.example.team12.virtualteacher.repositories.contracts.CourseRepository;
import com.example.team12.virtualteacher.repositories.contracts.UserRepository;
import com.example.team12.virtualteacher.services.contracts.UserService;
import com.example.team12.virtualteacher.services.utils.AuthorisationCheck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.example.team12.virtualteacher.services.utils.Constants.*;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private final CourseRepository courseRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           CourseRepository courseRepository) {
        this.userRepository = userRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public List<User> filter(FilterOptionsUser filterOptions, User user) {
        AuthorisationCheck.checkIfCanAccessTeacherInfo(user);
        return userRepository.filter(filterOptions);
    }

    @Override
    public List<User> getUsers(User user) {
        AuthorisationCheck.checkIfCanAccessAdminInfo(user);
        return userRepository.getUsers();
    }

    @Override
    public User getUserById(int id) {
        return userRepository.getById(id);
    }

    public List<User> getAllStudents(User user) {
        AuthorisationCheck.checkIfCanAccessTeacherInfo(user);
        return userRepository.getAllStudents();
    }

    public List<Course> getTeacherCreatedCourses(User user, int teacherId) {
        AuthorisationCheck.checkIfCanAccessTeacherInfo(user);
        return userRepository.getTeacherCreatedCourses(teacherId);
    }

    public List<Lecture> getStudentEnrolledLectures(User user, int studentId) {
        AuthorisationCheck.checkIfCanAccessStudentInfo(user, studentId);
        User studentToCheck = userRepository.getById(studentId);
        return studentToCheck.getStudentEnrolledLectures().stream().collect(Collectors.toList());

    }

    public void enrollOnCourse(User user, int courseId) {
        Course course = courseRepository.getById(courseId);
        LocalDate enrollDate = course.getStartDate();
        if (LocalDate.now().isBefore(enrollDate)) {
            throw new UnauthorizedOperationException
                    (CANNOT_ENROLL_BEFORE_SAID_DATE);
        }
        boolean duplicateExists = true;
        try {
            courseRepository.getById(courseId);
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
            throw new EntityNotFoundException("Course", "course title", course.getTitle());
        }
        if (user.getStudentEnrolledCourses().contains(course)) {
            throw new DuplicateEntityException("Course", "title", course.getTitle());
        }
        userRepository.enrollOnCourse(user, courseId);
    }

    @Override
    public List<Course> getStudentCompletedCourses(int studentId) {
        return userRepository.getStudentCompletedCourses(studentId);
    }

    public Map<String, Double> getStudentCompletedCoursesWithGrades(int studentId) {
        return userRepository.getStudentCompletedCoursesWithGrades(studentId);
    }

    @Override
    public User checkUserById(User user, int id) {
        User userToCheck = getUserById(id);
        if (userToCheck.checkIfUserStudent()) {
            AuthorisationCheck.checkIfCanAccessStudentInfo(user, id);
        } else if (userToCheck.checkIfUserTeacher()) {
            AuthorisationCheck.checkIfCanAccessAdminInfo(user, id);
        } else {
            AuthorisationCheck.checkIfCanAccessAdminInfo(user);
        }
        return userRepository.getById(id);
    }

    @Override
    public User createUser(User user) {
        boolean duplicateEmailExists = true;
        try {
            userRepository.getByField("email", user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateEmailExists = false;
        }

        if (duplicateEmailExists) {
            throw new DuplicateEntityException("User", "Email Address", user.getEmail());
        }
        userRepository.create(user);
        return user;
    }

    @Override
    public User updateUser(User user, User userWhoIsUpdating) {
        if (!userWhoIsUpdating.checkIfUserAdmin() && user.getId() != userWhoIsUpdating.getId()) {
            throw new UnauthorizedOperationException(EDIT_USER_ERROR_MESSAGE);
        }
        boolean duplicateEmailExists = true;

        try {
            User existingUserWithEmail = userRepository.getByField("email", user.getEmail());
            if (user.getId() == existingUserWithEmail.getId()) {
                duplicateEmailExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateEmailExists = false;
        }

        if (duplicateEmailExists) {
            throw new DuplicateEntityException("User", "Email Address", user.getEmail());
        }
        userRepository.update(user);
        return user;
    }

    @Override
    public void deleteUser(int id, User userWhoIsUpdating) {
        if (!userWhoIsUpdating.checkIfUserAdmin()) {
            throw new UnauthorizedOperationException(MODIFY_USER_ERROR_MESSAGE);
        }
        userRepository.delete(id);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByField("email", email);
    }

    @Override
    public void updateUserRole(int id, User userWhoIsUpdating, UserRole role) {
        if (!userWhoIsUpdating.checkIfUserAdmin()) {
            throw new UnauthorizedOperationException(MODIFY_USER_ERROR_MESSAGE);
        }
        User userToUpdate = getUserById(id);
        if (role.getName().equalsIgnoreCase("Admin")) {
            UserRole userRole = new UserRole(1, "Admin");
            userToUpdate.setRole(userRole);
        } else if (role.getName().equalsIgnoreCase("Teacher")) {
            UserRole userRole = new UserRole(3, "Teacher");
            userToUpdate.setRole(userRole);
        }
        userRepository.updateUserRole(userToUpdate, role);
    }

    @Override
    public boolean verify(String verificationCode) {
        User user = userRepository.findByVerificationCode(verificationCode);

        if (user == null || user.isEnabled()) {
            return false;
        } else {
            user.setVerificationCode(null);
            user.setEnabled(true);
            userRepository.update(user);

            return true;
        }
    }

    @Override
    public void updateResetPasswordToken(String token, String email) {
        User user = userRepository.getByField("email", email);
        if (user != null) {
            user.setResetPasswordToken(token);
            userRepository.update(user);
        } else {
            throw new EntityNotFoundException("Could not find any customer with the email " + email);
        }
    }

    @Override
    public User getByResetPasswordToken(String token) {
        return userRepository.findByResetPasswordToken(token);
    }

    @Override
    public void updatePassword(User user, String newPassword) {
        user.setPassword(newPassword);

        user.setResetPasswordToken(null);
        userRepository.update(user);
    }
}
