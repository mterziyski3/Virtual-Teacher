package com.example.team12.virtualteacher.models;

import javax.persistence.*;

//Course Rating – each course can be rated from 1 to 5 from the students.
//The comment on the rating is mandatory to the rating and it should be a minimum of 75 symbols.
@Entity
@Table(name = "courses_rating")
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "student_course_rating_id")
    private int id;

    @Column(name = "rating")
    private double rate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "student_id")
    private User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "course_id")
    private Course course;

    public Rating() {
    }

    public Rating(double rate, User user, Course course) {
        this.rate = rate;
        this.user = user;
        this.course = course;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}
