package com.example.team12.virtualteacher.mappers;

import com.example.team12.virtualteacher.models.Comment;
import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.models.dtos.CommentDto;
import com.example.team12.virtualteacher.services.contracts.CommentService;
import com.example.team12.virtualteacher.services.contracts.CourseService;
import com.example.team12.virtualteacher.services.contracts.UserService;
import org.springframework.stereotype.Component;

@Component
public class CommentMapper {

    private final CommentService commentService;

    private final UserService userService;

    private final CourseService courseService;


    public CommentMapper(CommentService commentService, UserService userService, CourseService courseService) {
        this.commentService = commentService;
        this.userService = userService;
        this.courseService = courseService;
    }

    public Comment dtoToObject(User user, Course course, CommentDto commentDto, Comment comment) {
        comment.setContent(commentDto.getContent());
        comment.setCreator(user);
        comment.setCourse(course);

        return comment;
    }
}
