package com.example.team12.virtualteacher.repositories;

import com.example.team12.virtualteacher.exceptions.EntityNotFoundException;
import com.example.team12.virtualteacher.models.Assignment;
import com.example.team12.virtualteacher.models.FilterOptionsLecture;
import com.example.team12.virtualteacher.models.Lecture;
import com.example.team12.virtualteacher.repositories.contracts.LectureRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Locale;

@Repository
public class LectureRepositoryImpl extends AbstractCRUDRepository<Lecture> implements LectureRepository {
    @Autowired
    public LectureRepositoryImpl(SessionFactory sessionFactory) {
        super(Lecture.class, sessionFactory);
    }

    @Override
    public List<Lecture> filter(FilterOptionsLecture filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from Lecture where 1=1");

            if (filterOptions.getTitle().isPresent()) {
                queryString.append(" and title like :title");
            }
            if (filterOptions.getDescription().isPresent()) {
                queryString.append(" and description like :description");
            }

            addSorting(filterOptions, queryString);

            Query<Lecture> query = session.createQuery(queryString.toString(), Lecture.class);

            if (filterOptions.getTitle().isPresent()) {
                // HQL: and name like %[some value from user]%
                query.setParameter("title", "%" + filterOptions.getTitle().get() + "%"); // %some-value%
            }
            if (filterOptions.getDescription().isPresent()) {
                query.setParameter("description", "%" + filterOptions.getDescription().get() + "%");
            }

            return query.list();
        }
    }

    private void addSorting(FilterOptionsLecture filterOptions, StringBuilder queryString) {
        if (filterOptions.getSortBy().isEmpty()) {
            return;
        }

        String orderBy = "";
        switch (filterOptions.getSortBy().get()) {
            case "title":
                orderBy = "title";
                break;
            case "description":
                orderBy = "description";
                break;
            default:
                return;
        }
        queryString.append(String.format(" order by %s", orderBy));

        if (filterOptions.getSortOrder().isPresent() && filterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            queryString.append(" desc");
        }
    }

    @Override
    public Lecture getLectureByTitle(String title) {
        try (Session session = sessionFactory.openSession()) {
            Query<Lecture> query = session.createQuery("from Lecture where title = :title", Lecture.class);
            query.setParameter("title", title);

            List<Lecture> result = query.list();

            if (result.size() == 0) {
                throw new EntityNotFoundException("Lecture", "title", title);
            }

            return result.get(0);
        }
    }

    @Override
    public void assessAssignment(Assignment assignment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(assignment);
            session.getTransaction().commit();
        }
    }


    private static boolean containsIgnoreCase(String value, String target) {
        return value.toLowerCase(Locale.ROOT).contains(target.toLowerCase());
    }
}
