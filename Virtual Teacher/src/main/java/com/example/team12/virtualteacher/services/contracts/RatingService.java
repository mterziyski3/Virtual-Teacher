package com.example.team12.virtualteacher.services.contracts;

import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.User;

public interface RatingService {

    void rate(Course courseToRate, User user, double rating);

    Double getAvgRating(Course course);

    boolean ifStudentRatedCourse(User user, Course course);
}
