package com.example.team12.virtualteacher.mappers;

import com.example.team12.virtualteacher.models.UserRole;
import com.example.team12.virtualteacher.models.dtos.UserRoleDto;
import org.springframework.stereotype.Component;

@Component
public class UserRoleMapper {

    public UserRole fromDto(UserRoleDto roleDto) {
        UserRole userRole = new UserRole();
        userRole.setName(roleDto.getName());
        return userRole;
    }
}
