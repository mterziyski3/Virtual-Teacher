package com.example.team12.virtualteacher.models;

import java.util.Optional;

public class FilterOptionsUser {
    private Optional<String> firstName;
    private Optional<String> lastName;
    private Optional<String> email;
    private Optional<String> sortBy;
    private Optional<String> sortOrder;

    public FilterOptionsUser(Optional<String> firstName,
                             Optional<String> lastName,
                             Optional<String> email,
                             Optional<String> sortBy,
                             Optional<String> sortOrder) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.sortBy = sortBy;
        this.sortOrder = sortOrder;
    }

    public Optional<String> getFirstName() {
        return firstName;
    }

    public Optional<String> getLastName() {
        return lastName;
    }

    public Optional<String> getEmail() {
        return email;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }
}
