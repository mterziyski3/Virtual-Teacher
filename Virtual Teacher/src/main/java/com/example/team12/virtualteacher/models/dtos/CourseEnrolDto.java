package com.example.team12.virtualteacher.models.dtos;

import javax.validation.constraints.Size;

public class CourseEnrolDto {

    @Size(min = 5, max = 50, message = "Title name should be between 5 and 50 symbols")
    private String title;

    public CourseEnrolDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
