package com.example.team12.virtualteacher.services.contracts;

import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.FilterOptionsCourse;
import com.example.team12.virtualteacher.models.User;

import java.util.List;

public interface CourseService {

    List<Course> getAll();

    List<Course> getPublished();

    List<Course> filter(FilterOptionsCourse filterOptionsCourse);

    List<Course> filterToPublished(FilterOptionsCourse filterOptionsCourse);

    Course getById(int id);

    Course create(Course course, User user);

    Course update(Course course, User user);

    void delete(int id, User user);

    Course getCourseByTitle(String title);
}
