package com.example.team12.virtualteacher.repositories;

import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.FilterOptionsCourse;
import com.example.team12.virtualteacher.repositories.contracts.CourseRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CourseRepositoryImpl extends AbstractCRUDRepository<Course> implements CourseRepository {

    @Autowired
    public CourseRepositoryImpl(SessionFactory sessionFactory) {
        super(Course.class, sessionFactory);
    }

    @Override
    public List<Course> getPublished() {
        try (Session session = sessionFactory.openSession()) {
            Query<Course> query = session.createQuery("from Course where status = 1", Course.class);
            return query.list();
        }
    }

    @Override
    public List<Course> filter(FilterOptionsCourse filterOptionsCourse) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from Course where 1=1");
            queryAppend(queryString, filterOptionsCourse);
            return querySetParameter(filterOptionsCourse, session, queryString);
        }
    }

    @Override
    public List<Course> filterToPublished(FilterOptionsCourse filterOptionsCourse) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from Course where status = 1");
            queryAppend(queryString, filterOptionsCourse);
            return querySetParameter(filterOptionsCourse, session, queryString);
        }
    }

    private void queryAppend(StringBuilder queryString, FilterOptionsCourse filterOptionsCourse) {
        if (filterOptionsCourse.getTitle().isPresent()) {
            queryString.append(" and title like :title");
        }
        if (filterOptionsCourse.getTopic().isPresent()) {
            queryString.append(" and topic like :topic");
        }
        if (filterOptionsCourse.getCreator().isPresent()) {
            queryString.append(" and creator.fullName like :creator");
        }
        if (filterOptionsCourse.getRating().isPresent()) {
            queryString.append(" and rating = :rating");
        }
        addSorting(filterOptionsCourse, queryString);

    }

    private List<Course> querySetParameter(FilterOptionsCourse filterOptionsCourse, Session session, StringBuilder queryString) {
        Query<Course> query = session.createQuery(queryString.toString(), Course.class);

        if (filterOptionsCourse.getTitle().isPresent()) {
            query.setParameter("title", String.format("%%%s%%", filterOptionsCourse.getTitle().get()));
        }
        if (filterOptionsCourse.getTopic().isPresent()) {
            query.setParameter("topic", String.format("%%%s%%", filterOptionsCourse.getTopic().get()));
        }
        if (filterOptionsCourse.getCreator().isPresent()) {
            query.setParameter("creator", String.format("%%%s%%", filterOptionsCourse.getCreator().get()));
        }
        if (filterOptionsCourse.getRating().isPresent()) {
            query.setParameter("rating", filterOptionsCourse.getRating().get());
        }
        return query.list();
    }


    private void addSorting(FilterOptionsCourse filterOptionsCourse, StringBuilder queryString) {
        if (filterOptionsCourse.getSortBy().isEmpty()) {
            return;
        }

        String orderBy = "";
        switch (filterOptionsCourse.getSortBy().get()) {
            case "title":
                orderBy = "title";
                break;
            case "topic":
                orderBy = "topic";
                break;
            case "rating":
                orderBy = "rating";
                break;
            default:
                return;
        }

        queryString.append(String.format(" order by %s", orderBy));

        if (filterOptionsCourse.getSortOrder().isPresent() &&
                filterOptionsCourse.getSortOrder().get().equalsIgnoreCase("desc")) {
            queryString.append(" desc");
        }
    }
}
