package com.example.team12.virtualteacher.controllers.mvc;

import com.example.team12.virtualteacher.controllers.AuthenticationHelper;
import com.example.team12.virtualteacher.exceptions.AuthenticationFailureException;
import com.example.team12.virtualteacher.exceptions.DuplicateEntityException;
import com.example.team12.virtualteacher.exceptions.EntityNotFoundException;
import com.example.team12.virtualteacher.exceptions.UnauthorizedOperationException;
import com.example.team12.virtualteacher.mappers.CommentMapper;
import com.example.team12.virtualteacher.mappers.CourseMapper;
import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.FilterOptionsCourse;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.models.dtos.*;
import com.example.team12.virtualteacher.services.contracts.CourseService;
import com.example.team12.virtualteacher.services.contracts.RatingService;
import com.example.team12.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/courses")
public class CourseMvcController {

    private final CourseService courseService;
    private final AuthenticationHelper authenticationHelper;
    private final CourseMapper courseMapper;

    private final CommentMapper commentMapper;
    private final RatingService ratingService;
    private final UserService userService;

    @Autowired
    public CourseMvcController(CourseService courseService, AuthenticationHelper authenticationHelper,
                               CourseMapper courseMapper, CommentMapper commentMapper, RatingService ratingService,
                               UserService userService) {
        this.courseService = courseService;
        this.authenticationHelper = authenticationHelper;
        this.courseMapper = courseMapper;
        this.commentMapper = commentMapper;
        this.ratingService = ratingService;
        this.userService = userService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession httpSession) {
        try {
            User user = authenticationHelper.tryGetUser(httpSession);
            return user.checkIfUserAdmin();
        } catch (AuthenticationFailureException e) {
            return false;
        }
    }

    @ModelAttribute("isTeacher")
    public boolean populateIsTeacher(HttpSession httpSession) {
        try {
            User user = authenticationHelper.tryGetUser(httpSession);
            return user.checkIfUserTeacher();
        } catch (AuthenticationFailureException e) {
            return false;
        }
    }

    @ModelAttribute("loggedUser")
    public User populateLoggedUser(HttpSession httpSession) {
        try {
            return authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return new User();
        }
    }

    @GetMapping
    public String showAllCourses(@ModelAttribute("filterOptions") CourseFilterDto courseFilterDto,
                                 Model model,
                                 HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (!user.checkIfUserAdmin() && !user.checkIfUserTeacher()) {
            return "forbidden";
        }

        FilterOptionsCourse filterOptionsCourse = courseMapper.fromFilterDto(courseFilterDto);
        List<Course> courses = courseService.filter(filterOptionsCourse);

        model.addAttribute("filterOptions", courseFilterDto);
        model.addAttribute("courses", courses);
        return "courses-view";
    }

    @GetMapping("/{id}")
    public String showSingleCourse(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Course courseEnrolled = courseService.getById(id);
            CourseDtoOut courseDtoOut = courseMapper.objectToDto(courseEnrolled);
            model.addAttribute("course", courseDtoOut);
            model.addAttribute("courseEnrolled", courseEnrolled);
            model.addAttribute("lecture", new LectureDto());
            model.addAttribute("currentUser", userService.getUserById(user.getId()));
            return "course-view";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/rate")
    public String showRateCoursePage(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        Course courseToRate = courseService.getById(id);
        Boolean ifStudentRatedCourse = ratingService.ifStudentRatedCourse(user, courseToRate);

        if (user.checkIfUserAdmin() && user.checkIfUserTeacher() || !ifStudentRatedCourse) {
            return "forbidden";
        }

        model.addAttribute("course", courseToRate);
        model.addAttribute("comment", new CommentDto());
        return "rate-course";
    }


    @PostMapping("/{id}/rate")
    public String RateCourse(@PathVariable int id,
                             Model model,
                             RatingDto ratingDto,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Course courseToRate = courseService.getById(id);
            Boolean ifStudentRatedCourse = ratingService.ifStudentRatedCourse(user, courseToRate);
            ratingService.rate(courseToRate, user, ratingDto.getRating());
            model.addAttribute("ifStudentRatedCourse", ifStudentRatedCourse);
            model.addAttribute("course", courseToRate);
            model.addAttribute("comment", new CommentDto());
            return "rate-course";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "forbidden";
        }
    }

    @GetMapping("/new")
    public String showNewCoursePage(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (!user.checkIfUserAdmin() && !user.checkIfUserTeacher()) {
            return "forbidden";
        }
        model.addAttribute("course", new CourseDto());
        return "course-new";
    }

    @PostMapping("/new")
    public String createCourse(@Valid @ModelAttribute("course") CourseDto courseDto,
                               BindingResult errors,
                               Model model,
                               HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (errors.hasErrors()) {
            return "course-new";
        }
        try {
            Course newCourse = courseMapper.dtoToObject(user, courseDto, new Course());
            newCourse.setCreator(newCourse.getCreator());
            courseService.create(newCourse, user);
            return "redirect:/courses";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("title", "course.exists", e.getMessage());
            return "course-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "forbidden";
        }
    }

    @GetMapping("/{id}/edit")
    public String showEditCoursePage(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (!user.checkIfUserAdmin() && !user.checkIfUserTeacher()) {
            return "forbidden";
        }

        try {
            Course course = courseService.getById(id);
            model.addAttribute("course", course);
            model.addAttribute("courseId", course.getId());
            return "course-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/edit")
    public String editCourse(@PathVariable int id, @Valid @ModelAttribute("course") CourseDto courseDto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (errors.hasErrors()) {
            return "course-update";
        }
        try {
            Course courseToUpdate = courseService.getById(courseDto.getId());
            Course course = courseMapper.dtoToObject(user, courseDto, courseToUpdate);

            courseService.update(course, user);
            model.addAttribute("courseId", course.getId());
            return "redirect:/courses";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "course.exists", e.getMessage());
            return "course-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "forbidden";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteCourse(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (!user.checkIfUserAdmin() && !user.checkIfUserTeacher()) {
            return "forbidden";
        }
        try {
            courseService.delete(id, user);
            return "redirect:/courses";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "forbidden";
        }
    }

    @GetMapping("/{id}/enrol")
    public String enrollOnCourse(@PathVariable int id,
                                 Model model,
                                 HttpSession httpSession) {
        try {
            User user = authenticationHelper.tryGetUser(httpSession);
            userService.enrollOnCourse(user, id);
            model.addAttribute("user", user);
            model.addAttribute("courseId", id);
            return String.format("redirect:/courses/%d", id);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "forbidden";
        }
    }
}
