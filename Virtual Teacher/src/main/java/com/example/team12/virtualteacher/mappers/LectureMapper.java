package com.example.team12.virtualteacher.mappers;

import com.example.team12.virtualteacher.models.*;
import com.example.team12.virtualteacher.models.dtos.AssignmentDto;
import com.example.team12.virtualteacher.models.dtos.LectureDto;
import com.example.team12.virtualteacher.models.dtos.LectureFilterDto;
import com.example.team12.virtualteacher.services.contracts.CourseService;
import com.example.team12.virtualteacher.services.contracts.LectureService;
import com.example.team12.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class LectureMapper {

    private final UserService userService;
    private final LectureService lectureService;
    private final CourseService courseService;

    @Autowired
    public LectureMapper(UserService userService, LectureService lectureService, CourseService courseService) {
        this.userService = userService;
        this.lectureService = lectureService;
        this.courseService = courseService;
    }

    public Lecture fromDto(LectureDto lectureDto, Lecture lecture) {
        lecture.setTitle(lectureDto.getTitle());
        lecture.setDescription(lectureDto.getDescription());
        Course course = courseService.getCourseByTitle(lectureDto.getCourseTitle());
        lecture.setCourse(course);

        return lecture;
    }

    public LectureDto toDto(Lecture lecture) {
        LectureDto lectureDto = new LectureDto();
        lectureDto.setId(lecture.getId());
        lectureDto.setTitle(lecture.getTitle());
        lectureDto.setDescription(lecture.getDescription());

        return lectureDto;
    }

    public FilterOptionsLecture fromFilterDto(LectureFilterDto lectureFilterDto) {
        FilterOptionsLecture filterOptionsLecture = new FilterOptionsLecture(
                Optional.ofNullable(lectureFilterDto.getTitle()),
                Optional.ofNullable(lectureFilterDto.getDescription()),
                Optional.ofNullable(lectureFilterDto.getSortBy()),
                Optional.ofNullable(lectureFilterDto.getSortOrder())
        );

        return filterOptionsLecture;
    }

    public Assignment fromAssignmentDto(AssignmentDto assignmentDto) {
        Assignment assignment = new Assignment();

        User user = userService.getByEmail(assignmentDto.getUserEmail());
        assignment.setUser(user);

        Lecture lecture = lectureService.getById(assignmentDto.getLectureId());
        assignment.setLecture(lecture);

        assignment.setGrade(assignmentDto.getGrade());

        return assignment;
    }
}
