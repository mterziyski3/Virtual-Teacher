package com.example.team12.virtualteacher.repositories;

import com.example.team12.virtualteacher.models.*;
import com.example.team12.virtualteacher.repositories.contracts.AssignmentRepository;
import com.example.team12.virtualteacher.repositories.contracts.CourseRepository;
import com.example.team12.virtualteacher.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class UserRepositoryImpl extends AbstractCRUDRepository<User> implements UserRepository {

    private final CourseRepository courseRepository;
    private final AssignmentRepository assignmentRepository;


    @Autowired
    public UserRepositoryImpl(CourseRepository courseRepository,
                              SessionFactory sessionFactory,
                              AssignmentRepository assignmentRepository) {
        super(User.class, sessionFactory);
        this.courseRepository = courseRepository;
        this.assignmentRepository = assignmentRepository;
    }

    @Override
    public List<User> filter(FilterOptionsUser filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from User where 1=1");

            if (filterOptions.getFirstName().isPresent()) {
                queryString.append(" and firstName like :firstName");
            }
            if (filterOptions.getLastName().isPresent()) {
                queryString.append(" and lastName like :lastName");
            }
            if (filterOptions.getEmail().isPresent()) {
                queryString.append(" and email like :email");
            }
            addSorting(filterOptions, queryString);

            Query<User> query = session.createQuery(queryString.toString(), User.class);

            if (filterOptions.getFirstName().isPresent()) {
                query.setParameter("firstName", "%" + filterOptions.getFirstName().get() + "%");
            }
            if (filterOptions.getLastName().isPresent()) {
                query.setParameter("lastName", "%" + filterOptions.getLastName().get() + "%");
            }
            if (filterOptions.getEmail().isPresent()) {
                query.setParameter("email", "%" + filterOptions.getEmail().get() + "%");
            }

            return query.list();
        }
    }


    private void addSorting(FilterOptionsUser filterOptions, StringBuilder queryString) {
        if (filterOptions.getSortBy().isEmpty()) {
            return;
        }

        String orderBy = "";
        switch (filterOptions.getSortBy().get()) {
            case "firstName":
                orderBy = "firstName";
                break;
            case "lastName":
                orderBy = "lastName";
                break;
            case "email":
                orderBy = "email";
                break;
            default:
                return;
        }
        queryString.append(String.format(" order by %s", orderBy));

        if (filterOptions.getSortOrder().isPresent() &&
                filterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            queryString.append(" desc");
        }
    }

    @Override
    public List<User> getUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);
            return query.list();
        }
    }

    public List<User> getAllStudents() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery
                    ("from User where role.name='student'", User.class);
            return query.list();
        }
    }

    public List<Course> getTeacherCreatedCourses(int creatorId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Course> query = session.createQuery
                    ("from Course where creator.id = :creatorId", Course.class);
            query.setParameter("creatorId", creatorId);
            return query.list();
        }
    }

    public List<Course> getStudentCompletedCourses(int studentId) {
        User student = getById(studentId);
        List<Course> studentEnrolledCourses = new ArrayList<>(student.getStudentEnrolledCourses());
        List<Course> completedCourses = new ArrayList<>();
        for (int i = 0; i < studentEnrolledCourses.size(); i++) {
            if (studentEnrolledCourses.get(i).getPassingGrade() <=
                    assignmentRepository.getStudentAvgCourseGrade(student, studentEnrolledCourses.get(i))) {
                completedCourses.add(studentEnrolledCourses.get(i));
            }
        }
        return completedCourses;
    }

    public Map<String, Double> getStudentCompletedCoursesWithGrades(int studentId) {
        User student = getById(studentId);
        List<Course> studentEnrolledCourses = new ArrayList<>(student.getStudentEnrolledCourses());
        Map<String, Double> completedCoursesWithGrades = new HashMap<>();
        for (int i = 0; i < studentEnrolledCourses.size(); i++) {
            double courseAvgGrade = assignmentRepository.getStudentAvgCourseGrade(student,
                    studentEnrolledCourses.get(i));
            if (studentEnrolledCourses.get(i).getPassingGrade() <=
                    courseAvgGrade) {
                completedCoursesWithGrades.put(studentEnrolledCourses.get(i).getTitle(),
                        courseAvgGrade);
            }
        }
        return completedCoursesWithGrades;
    }


    public void enrollOnCourse(User user, int courseId) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Course course = courseRepository.getById(courseId);
            user.getStudentEnrolledCourses().add(course);
            Set<Lecture> courseLectures = course.getLectures();
            user.getStudentEnrolledLectures().addAll(courseLectures);
            session.saveOrUpdate(user);
            session.saveOrUpdate(course);
            session.getTransaction().commit();
        }
    }


    @Override
    public void updateUserRole(User userToUpdate, UserRole role) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.saveOrUpdate(userToUpdate);
            session.getTransaction().commit();
        }
    }

    @Override
    public User findByVerificationCode(String code) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query =
                    session.createQuery("from User where verificationCode = :code", User.class);
            query.setParameter("code", code);
            List<User> result = query.list();
            return result.get(0);
        }
    }

    @Override
    public User findByResetPasswordToken(String token) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query =
                    session.createQuery("from User where resetPasswordToken = :token", User.class);
            query.setParameter("token", token);
            List<User> result = query.list();
            return result.get(0);
        }
    }


    private static boolean containsIgnoreCase(String value, String target) {
        return value.toLowerCase(Locale.ROOT).contains(target.toLowerCase());
    }

    public boolean hasInStudentEnrolledCourses(User user, int courseId) {
        Course course = courseRepository.getByField("id", courseId);
        return user.getStudentEnrolledCourses().contains(course);
    }
}
