package com.example.team12.virtualteacher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VirtualTeacherApplication {

    public static void main(String[] args) {
        SpringApplication.run(VirtualTeacherApplication.class, args);
    }

}
