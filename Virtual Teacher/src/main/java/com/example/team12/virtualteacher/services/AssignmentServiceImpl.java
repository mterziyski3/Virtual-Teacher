package com.example.team12.virtualteacher.services;

import com.example.team12.virtualteacher.models.Assignment;
import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.repositories.contracts.AssignmentRepository;
import com.example.team12.virtualteacher.repositories.contracts.CourseRepository;
import com.example.team12.virtualteacher.repositories.contracts.UserRepository;
import com.example.team12.virtualteacher.services.contracts.AssignmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AssignmentServiceImpl implements AssignmentService {

    private final AssignmentRepository assignmentRepository;
    private final UserRepository userRepository;

    private final CourseRepository courseRepository;

    @Autowired
    public AssignmentServiceImpl(AssignmentRepository assignmentRepository, UserRepository userRepository, CourseRepository courseRepository) {
        this.assignmentRepository = assignmentRepository;
        this.userRepository = userRepository;
        this.courseRepository = courseRepository;
    }


    @Override
    public boolean studentHasPassedCourse(User user, Course course) {
        return assignmentRepository.studentHasPassedCourse(user, course);
    }

    @Override
    public Double getStudentAvgCourseGrade(int studentId, int courseId) {
        User student = userRepository.getById(studentId);
        Course course = courseRepository.getById(courseId);
        return assignmentRepository.getStudentAvgCourseGrade(student, course);
    }

    @Override
    public List<Integer> getStudentGrades(User user, Course course) {
        return assignmentRepository.getStudentGrades(user, course);
    }

    public List<Assignment> getStudentAssignment(User user, Course course) {
        return assignmentRepository.getStudentAssignment(user, course);
    }
}
