package com.example.team12.virtualteacher.controllers.mvc;

import com.example.team12.virtualteacher.controllers.AuthenticationHelper;
import com.example.team12.virtualteacher.exceptions.AuthenticationFailureException;
import com.example.team12.virtualteacher.exceptions.DuplicateEntityException;
import com.example.team12.virtualteacher.mappers.UserMapper;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.models.dtos.LoginDto;
import com.example.team12.virtualteacher.models.dtos.RegisterDto;
import com.example.team12.virtualteacher.services.contracts.UserService;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;

@Controller
@RequestMapping("/auth")
public class AuthenticationController {
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;
    private final JavaMailSender mailSender;
    private final String siteURL = "http://localhost:8080";

    @Autowired
    public AuthenticationController(UserService userService,
                                    AuthenticationHelper authenticationHelper,
                                    UserMapper userMapper, JavaMailSender mailSender) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
        this.mailSender = mailSender;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDto());
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto loginDto, BindingResult bindingResult, HttpSession httpSession) {
        if (bindingResult.hasErrors()) {
            return "login";
        }

        try {
            authenticationHelper.verifyAuthentication(loginDto.getEmail(), loginDto.getPassword());
            httpSession.setAttribute("currentUser", loginDto.getEmail());

            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("email", "auth_error", e.getMessage());

            return "login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession httpSession) {
        httpSession.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterDto());

        return "register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register")
                                 RegisterDto registerDto, BindingResult bindingResult, HttpSession httpSession)
            throws UnsupportedEncodingException, MessagingException {
        if (bindingResult.hasErrors()) {
            return "register";
        }

        if (!registerDto.getPassword().equals(registerDto.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error",
                    "Password confirmation should match password.");
            return "register";
        }

        try {
            User user = userMapper.fromRegisterDto(registerDto);

            String randomCode = RandomString.make(64);
            user.setVerificationCode(randomCode);
            user.setEnabled(false);

            userService.createUser(user);

            sendVerificationEmail(user);

            return "redirect:/auth/login";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("email", "duplicateEmail", e.getMessage());

            return "register";
        }
    }

    private void sendVerificationEmail(User user) throws MessagingException, UnsupportedEncodingException {
        String toAddress = user.getEmail();
        String fromAddress = "team12vteacher@zohomail.eu";
        String senderName = "Team 12";
        String subject = "Please verify your registration";
        String content = "Dear [[name]],<br>"
                + "Please click the link below to verify your registration:<br>"
                + "<h3><a href=\"[[URL]]\" target=\"_self\">VERIFY</a></h3>"
                + "Thank you,<br>"
                + "Team 12.";

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom(fromAddress, senderName);
        helper.setTo(toAddress);
        helper.setSubject(subject);

        content = content.replace("[[name]]", user.getFirstName());
        String verifyURL = siteURL + "/users/verify?code=" + user.getVerificationCode();

        content = content.replace("[[URL]]", verifyURL);

        helper.setText(content, true);

        mailSender.send(message);
    }

}
