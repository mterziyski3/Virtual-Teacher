package com.example.team12.virtualteacher.controllers.mvc;

import com.example.team12.virtualteacher.controllers.AuthenticationHelper;
import com.example.team12.virtualteacher.exceptions.AuthenticationFailureException;
import com.example.team12.virtualteacher.exceptions.DuplicateEntityException;
import com.example.team12.virtualteacher.exceptions.EntityNotFoundException;
import com.example.team12.virtualteacher.exceptions.UnauthorizedOperationException;
import com.example.team12.virtualteacher.mappers.LectureMapper;
import com.example.team12.virtualteacher.models.Assignment;
import com.example.team12.virtualteacher.models.FilterOptionsLecture;
import com.example.team12.virtualteacher.models.Lecture;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.models.dtos.AssignmentDto;
import com.example.team12.virtualteacher.models.dtos.LectureDto;
import com.example.team12.virtualteacher.models.dtos.LectureFilterDto;
import com.example.team12.virtualteacher.services.contracts.CourseService;
import com.example.team12.virtualteacher.services.contracts.LectureService;
import com.example.team12.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/lectures")
public class LectureMvcController {

    private String uploadFolder = "/resources";

    public static String directoryStudentAssignments = System.getProperty("user.dir") + "/uploads/students";
    public static final String LECTURE_WITH_THIS_TITLE_ALREADY_EXISTS = "Lecture wit this title already exists.";
    private final LectureService lectureService;
    private final LectureMapper lectureMapper;
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final CourseService courseService;


    @Autowired
    public LectureMvcController(LectureService lectureService, LectureMapper lectureMapper,
                                AuthenticationHelper authenticationHelper, UserService userService, CourseService courseService) {
        this.lectureService = lectureService;
        this.lectureMapper = lectureMapper;
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.courseService = courseService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession httpSession) {
        return httpSession.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession httpSession) {
        try {
            User user = authenticationHelper.tryGetUser(httpSession);
            return user.checkIfUserAdmin();
        } catch (AuthenticationFailureException e) {
            return false;
        }
    }

    @ModelAttribute("isStudent")
    public boolean populateIsStudent(HttpSession httpSession) {
        try {
            User user = authenticationHelper.tryGetUser(httpSession);
            return user.checkIfUserStudent();
        } catch (AuthenticationFailureException e) {
            return false;
        }
    }

    @ModelAttribute("loggedUser")
    public User populateLoggedUser(HttpSession httpSession) {
        try {
            return authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return new User();
        }
    }

    @GetMapping
    public String showLectures(@ModelAttribute("filterOptions") LectureFilterDto lectureFilterDto, Model model) {
        FilterOptionsLecture filterOptions = lectureMapper.fromFilterDto(lectureFilterDto);
        List<Lecture> lectures = lectureService.filter(filterOptions);

        model.addAttribute("filterOptions", lectureFilterDto);
        model.addAttribute("lectures", lectures);
        return "lectures-view";
    }


    @GetMapping("/{id}")
    public String showLecture(@PathVariable int id, Model model, HttpSession httpSession) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Lecture lecture = lectureService.getLecture(id);

            if (!user.getStudentEnrolledCourses().contains(lecture.getCourse()) && !user.checkIfUserAdmin()
                    && !lecture.getCourse().getCreator().equals(user)) {
                return "forbidden";
            }

            model.addAttribute("lecture", lecture);
            model.addAttribute("lectureId", lecture.getId());
            model.addAttribute("currentUser", user);
            model.addAttribute("assignment", new AssignmentDto());
            model.addAttribute("studentsWithAssignments", getAllUsersFromAssignments());
            return "lecture-view";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/new")
    public String showNewLecturePage(Model model, HttpSession httpSession) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (user.getRole().getName().equals("Student")) {
            return "forbidden";
        }

        model.addAttribute("lecture", new LectureDto());
        model.addAttribute("courses", courseService.getAll());

        return "lecture-new";
    }

    @PostMapping("/new")
    public String createLecture(@Valid @ModelAttribute("lecture") LectureDto lectureDto, BindingResult bindingResult, Model model, HttpSession httpSession) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "Lecture-New";
        }

        try {
            Lecture lecture = lectureMapper.fromDto(lectureDto, new Lecture());
            lectureService.createLecture(lecture);

            return "redirect:/lectures";
        } catch (DuplicateEntityException e) {
            if (e.getMessage().contains("Title")) {
                bindingResult.rejectValue("title", "duplicateLecture", LECTURE_WITH_THIS_TITLE_ALREADY_EXISTS);
            }

            return "lecture-new";
        }
    }

    @GetMapping("/{id}/edit")
    public String showEditLecturePage(@PathVariable int id, Model model, HttpSession httpSession) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (user.getRole().getName().equals("Student")) {
            return "forbidden";
        }

        try {
            Lecture lecture = lectureService.getById(id);
            LectureDto lectureDto = lectureMapper.toDto(lecture);
            model.addAttribute("lectureId", lecture.getId());
            model.addAttribute("lecture", lectureDto);
            model.addAttribute("courses", courseService.getAll());

            return "lecture-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/edit")
    public String editLecture(@PathVariable int id, @Valid @ModelAttribute("lecture") LectureDto lectureDto,
                              BindingResult bindingResult, Model model, HttpSession httpSession) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "lecture-update";
        }

        try {
            Lecture lecture = lectureMapper.fromDto(lectureDto, new Lecture());
            lecture.setId(id);
            lectureService.updateLecture(lecture, user);

            return "redirect:/lectures";
        } catch (DuplicateEntityException e) {
            if (e.getMessage().contains("Title")) {
                bindingResult.rejectValue("title", "duplicateLecture", LECTURE_WITH_THIS_TITLE_ALREADY_EXISTS);
            }

            return "lecture-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";

        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());

            return "forbidden";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteLecture(@PathVariable int id, Model model, HttpSession httpSession) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            lectureService.deleteLecture(id, user);

            return "redirect:/lectures";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "forbidden";
        }
    }

    @PostMapping("/assess/{id}")
    public String assessGrade(@PathVariable int id, @ModelAttribute("assignment") AssignmentDto assignmentDto,
                              RedirectAttributes redirectAttributes) {
        Assignment assignment = lectureMapper.fromAssignmentDto(assignmentDto);
        Lecture lecture = lectureService.getById(id);
        assignment.setLecture(lecture);
        assignment.setStatusId(1);

        lectureService.assessAssignment(assignment);

        redirectAttributes.addAttribute("id", lecture.getId());
        return "redirect:/lectures/{id}";
    }

    private File[] getStudentAssignments() {
        File folder = new File(directoryStudentAssignments);
        File[] listOfFiles = folder.listFiles();

        for (File file : listOfFiles) {
            if (file.isFile()) {
                System.out.println(file.getName());
            }
        }

        return listOfFiles;
    }

    private List<User> getAllUsersFromAssignments() {
        File[] listOfFiles = getStudentAssignments();
        List<User> usersWhichHaveAssignment = new ArrayList<>();

        for (File file : listOfFiles) {
            StringBuilder builder = new StringBuilder();

            String fileName = file.getName().toString();
            int startIndex = fileName.indexOf("-");
            int stopIndex = fileName.lastIndexOf(".");

            for (int i = startIndex + 1; i < stopIndex; i++) {
                builder.append(fileName.charAt(i));
            }

            String userEmail = builder.toString();

            usersWhichHaveAssignment.add(userService.getByEmail(userEmail));
        }

        return usersWhichHaveAssignment;
    }
}
