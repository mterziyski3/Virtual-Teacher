package com.example.team12.virtualteacher.models.dtos;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class RegisterDto extends LoginDto {
    @NotEmpty(message = "Password confirmation can't be empty")
    @Size(min = 8)
    private String passwordConfirm;

    @NotEmpty(message = "First name can't be empty")
    @Size(min = 2, max = 20, message = "First name's length should be between 2 and 20 symbols.")
    private String firstName;

    @NotEmpty(message = "Last name can't be empty")
    @Size(min = 2, max = 20, message = "Last name's length should be between 2 and 20 symbols.")

    private String lastName;

    private String requestedRoleName;

    public RegisterDto() {
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRequestedRoleName() {
        return requestedRoleName;
    }

    public void setRequestedRoleName(String requestedRoleName) {
        this.requestedRoleName = requestedRoleName;
    }
}
