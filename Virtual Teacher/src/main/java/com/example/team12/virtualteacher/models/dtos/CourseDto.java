package com.example.team12.virtualteacher.models.dtos;


import com.example.team12.virtualteacher.models.enums.CourseStatusType;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class CourseDto {

    private int id;

    @NotNull
    @Size(min = 5, max = 50, message = "Title name should be between 5 and 50 symbols")
    private String title;

    @NotNull
    @Size(min = 5, max = 30, message = "Topic name should be between 5 and 50 symbols")
    private String topic;

    @Size(min = 0, max = 1000, message = "Description should be max 1000 symbols")
    private String description;

    @NotNull(message = "Date can't be empty")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @NotNull(message = "Status can't be empty")
    private CourseStatusType status;

    public CourseDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public CourseStatusType getStatus() {
        return status;
    }

    public void setStatus(CourseStatusType status) {
        this.status = status;
    }
}
