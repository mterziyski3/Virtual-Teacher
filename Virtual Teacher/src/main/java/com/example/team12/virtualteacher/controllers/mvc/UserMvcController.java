package com.example.team12.virtualteacher.controllers.mvc;

import com.example.team12.virtualteacher.controllers.AuthenticationHelper;
import com.example.team12.virtualteacher.exceptions.AuthenticationFailureException;
import com.example.team12.virtualteacher.exceptions.DuplicateEntityException;
import com.example.team12.virtualteacher.exceptions.EntityNotFoundException;
import com.example.team12.virtualteacher.exceptions.UnauthorizedOperationException;
import com.example.team12.virtualteacher.mappers.CourseMapper;
import com.example.team12.virtualteacher.mappers.UserMapper;
import com.example.team12.virtualteacher.models.Assignment;
import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.FilterOptionsUser;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.models.dtos.UserDto;
import com.example.team12.virtualteacher.models.dtos.UserFilterDto;
import com.example.team12.virtualteacher.services.contracts.AssignmentService;
import com.example.team12.virtualteacher.services.contracts.CourseService;
import com.example.team12.virtualteacher.services.contracts.RatingService;
import com.example.team12.virtualteacher.services.contracts.UserService;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.example.team12.virtualteacher.services.utils.Constants.USER_WITH_THIS_EMAIL_ADDRESS_ALREADY_EXISTS;

@Controller
@RequestMapping("/users")
public class UserMvcController {
    private final UserService userService;
    private final UserMapper userMapper;
    private final CourseService courseService;
    private final AssignmentService assignmentService;
    private final RatingService ratingService;
    private final AuthenticationHelper authenticationHelper;
    private final JavaMailSender mailSender;
    private final String siteURL = "http://localhost:8080";


    @Autowired
    public UserMvcController(UserService userService,
                             UserMapper userMapper,
                             CourseMapper courseMapper,
                             CourseService courseService,
                             RatingService ratingService,
                             AssignmentService assignmentService,
                             AuthenticationHelper authenticationHelper,
                             JavaMailSender mailSender) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.courseService = courseService;
        this.assignmentService = assignmentService;
        this.ratingService = ratingService;
        this.authenticationHelper = authenticationHelper;
        this.mailSender = mailSender;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession httpSession) {
        return httpSession.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession httpSession) {
        try {
            User user = authenticationHelper.tryGetUser(httpSession);
            return user.checkIfUserAdmin();
        } catch (AuthenticationFailureException e) {
            return false;
        }
    }

    @ModelAttribute("isTeacher")
    public boolean populateIsTeacher(HttpSession httpSession) {
        try {
            User user = authenticationHelper.tryGetUser(httpSession);
            return user.checkIfUserTeacher();
        } catch (AuthenticationFailureException e) {
            return false;
        }
    }

    @ModelAttribute("isStudent")
    public boolean populateIsStudent(HttpSession httpSession) {
        try {
            User user = authenticationHelper.tryGetUser(httpSession);
            return user.checkIfUserStudent();
        } catch (AuthenticationFailureException e) {
            return false;
        }
    }

    @ModelAttribute("loggedUser")
    public User populateLoggedUser(HttpSession httpSession) {
        try {
            return authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return new User();
        }
    }

    @GetMapping
    public String showAllUsers(@ModelAttribute("filterOptions") UserFilterDto userFilterDto,
                               Model model,
                               HttpSession httpSession) {
        User userWhoIsChecking;

        try {
            userWhoIsChecking = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            List<User> allUsers = userService.getUsers(userWhoIsChecking);
            FilterOptionsUser filterOptions = userMapper.fromFilterDto(userFilterDto);
            List<User> users = userService.filter(filterOptions, userWhoIsChecking);
            model.addAttribute("allUsers", allUsers);
            model.addAttribute("filterOptions", userFilterDto);
            model.addAttribute("users", users);
            return "users-view";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "forbidden";
        }
    }

    @GetMapping("/students")
    public String showAllStudents(@ModelAttribute("filterOptions") UserFilterDto userFilterDto,
                                  Model model,
                                  HttpSession httpSession) {
        User userWhoIsChecking;
        try {
            userWhoIsChecking = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            List<User> allStudents = userService.getAllStudents(userWhoIsChecking);
            FilterOptionsUser filterOptions = userMapper.fromFilterDto(userFilterDto);
            List<User> users = userService.filter(filterOptions, userWhoIsChecking);
            model.addAttribute("allUsers", allStudents);
            model.addAttribute("filterOptions", userFilterDto);
            model.addAttribute("users", users);
            return "students-view";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "forbidden";
        }
    }

    @GetMapping("/{id}")
    public String checkUserById(@PathVariable @Valid int id,
                                Model model,
                                HttpSession httpSession) {
        try {
            User userWhoIsChecking = authenticationHelper.tryGetUser(httpSession);
            User user = userService.checkUserById(userWhoIsChecking, id);
            model.addAttribute("user", user);
            model.addAttribute("userWhoIsChecking", userWhoIsChecking);
            model.addAttribute("userId", user.getId());
            Map<String, Double> studentCompletedCoursesWithGrades =
                    userService.getStudentCompletedCoursesWithGrades(id);
            Set<Map.Entry<String, Double>> set = studentCompletedCoursesWithGrades.entrySet();
            List<Map.Entry<String, Double>> list = new ArrayList<Map.Entry<String, Double>>(set);
            model.addAttribute
                    ("studentCompletedCoursesWithGrades", userService.getStudentCompletedCoursesWithGrades(id));
            if (!user.checkIfUserStudent()) {
                List<Course> teacherCreatedCourses = userService.getTeacherCreatedCourses(userWhoIsChecking, id);
                model.addAttribute
                        ("teacherCreatedCourses", userService.getTeacherCreatedCourses(userWhoIsChecking, id));
            }
            return "user-view";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "forbidden";
        }
    }


    @GetMapping("/new")
    public String showNewUserPage(Model model,
                                  HttpSession httpSession) {
        try {
            authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("user", new UserDto());
        return "user-new";
    }

    @PostMapping("/new")
    public String createUser(@Valid @ModelAttribute("user") UserDto userDto,
                             BindingResult bindingResult,
                             Model model,
                             HttpSession httpSession) {
        try {
            authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "user-new";
        }

        try {
            User user = userMapper.fromDto(userDto);
            userService.createUser(user);

            return "redirect:/users";
        } catch (DuplicateEntityException e) {
            if (e.getMessage().contains("Email")) {
                bindingResult.rejectValue("email", "duplicateUser",
                        USER_WITH_THIS_EMAIL_ADDRESS_ALREADY_EXISTS);
            }
            return "user-name";
        }
    }

    @GetMapping("/{id}/edit")
    public String showEditUserPage(@PathVariable int id,
                                   Model model,
                                   HttpSession httpSession) {
        User updatingUser;
        try {
            updatingUser = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (!updatingUser.checkIfUserAdmin() & updatingUser.getId() != id) {
            return "forbidden";
        }

        try {
            User user = userService.getUserById(id);
            List<String> roles = new ArrayList<>();
            roles.add("Admin");
            roles.add("Teacher");
            roles.add("Student");
            UserDto userDto = userMapper.toDto(user);
            model.addAttribute("userId", id);
            model.addAttribute("user", userDto);
            model.addAttribute("roles", roles);
            return "user-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "forbidden";
        }
    }

    @PostMapping("/{id}/edit")
    public String editUser(@PathVariable int id, @Valid @ModelAttribute("user") UserDto userDto,
                           BindingResult bindingResult,
                           Model model, HttpSession httpSession) {
        User userWhoIsUpdating;
        try {
            userWhoIsUpdating = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (bindingResult.hasErrors()) {
            return "user-update";
        }
        try {
            User user = userMapper.fromDto(userDto, id);
            if (userWhoIsUpdating.getId() == user.getId()) {
                authenticationHelper.trySetUser(httpSession, user.getEmail());
            }
            userService.updateUser(user, userWhoIsUpdating);
            return "redirect:/users/{id}";
        } catch (DuplicateEntityException e) {
            if (e.getMessage().contains("Email")) {
                bindingResult.rejectValue("email", "duplicateUser",
                        USER_WITH_THIS_EMAIL_ADDRESS_ALREADY_EXISTS);
            }
            return "user-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "forbidden";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id, Model model, HttpSession httpSession) {
        User userWhoIsDeleting;
        try {
            userWhoIsDeleting = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            userService.deleteUser(id, userWhoIsDeleting);
            return "redirect:/users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "forbidden";
        }

    }

    @GetMapping("/verify")
    public String verifyUser(@RequestParam("code") String code) {
        if (userService.verify(code)) {
            return "verify_success";
        } else {
            return "verify_fail";
        }
    }

    @GetMapping("/forgot_password")
    public String showForgotPasswordForm() {
        return "forgot_password_form";
    }

    @PostMapping("/forgot_password")
    public String processForgotPassword(HttpServletRequest request,
                                        Model model) throws MessagingException,
            UnsupportedEncodingException {
        String email = request.getParameter("email");
        String token = RandomString.make(30);

        User user = userService.getByEmail(email);

        String toAddress = email;
        String fromAddress = "team12vteacher@zohomail.eu";
        String senderName = "Team 12";
        String subject = "You have requested password reset";
        String content = "Dear [[name]],<br>" +
                "Please click the link below to reset your password:<br>" +
                "<h3><a href=\"[[URL]]\" target=\"_self\">RESET</a></h3>" + "Thank you,<br>" + "Team 12.";

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom(fromAddress, senderName);
        helper.setTo(toAddress);
        helper.setSubject(subject);

        content = content.replace("[[name]]", user.getFirstName());

        String resetPasswordLink = siteURL + "/users/reset_password?token=" + token;
        content = content.replace("[[URL]]", resetPasswordLink);

        helper.setText(content, true);

        try {
            userService.updateResetPasswordToken(token, email);
            mailSender.send(message);

            model.addAttribute("message",
                    "We have sent a reset password link to your email. Please check.");

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
        }

        return "forgot_password_form";
    }

    @GetMapping("/reset_password")
    public String showResetPasswordForm(@RequestParam(value = "token") String token, Model model) {
        User user = userService.getByResetPasswordToken(token);
        model.addAttribute("token", token);

        if (user == null) {
            model.addAttribute("reset_password_message", "Invalid Token");
            return "reset_password_message";
        }

        return "reset_password_form";
    }

    @PostMapping("/reset_password")
    public String processResetPassword(HttpServletRequest request, Model model) {
        String token = request.getParameter("token");
        String password = request.getParameter("password");

        User user = userService.getByResetPasswordToken(token);
        model.addAttribute("title", "Reset your password");

        if (user == null) {
            model.addAttribute("reset_password_message", "Invalid Token");
            return "reset_password_message";
        } else {
            userService.updatePassword(user, password);

            model.addAttribute("reset_password_message",
                    "You have successfully changed your password.");
        }
        return "reset_password_message";
    }

    @GetMapping("/student-course/{courseId}")
    public String getStudentCourseInfo(@PathVariable int courseId,
                                       Model model,
                                       HttpSession httpSession) {
        try {
            User user = authenticationHelper.tryGetUser(httpSession);
            Course course = courseService.getById(courseId);
            Double avgGrade = assignmentService.getStudentAvgCourseGrade(user.getId(), courseId);
            Boolean hasPassedCourse = assignmentService.studentHasPassedCourse(user, course);
            Boolean ifStudentRatedCourse = ratingService.ifStudentRatedCourse(user, course);
            List<Assignment> assignments = assignmentService.getStudentAssignment(user, course);
            model.addAttribute("user", user);
            model.addAttribute("course", course);
            model.addAttribute("avgCourseGrade", avgGrade);
            model.addAttribute("hasPassedCourse", hasPassedCourse);
            model.addAttribute("assignments", assignments);
            model.addAttribute("ifStudentRatedCourse", ifStudentRatedCourse);
            return "student-course";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "forbidden";
        }
    }
}
