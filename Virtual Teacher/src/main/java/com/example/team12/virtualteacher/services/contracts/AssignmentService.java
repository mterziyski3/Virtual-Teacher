package com.example.team12.virtualteacher.services.contracts;

import com.example.team12.virtualteacher.models.Assignment;
import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.User;

import java.util.List;

public interface AssignmentService {

    boolean studentHasPassedCourse(User user, Course course);

    Double getStudentAvgCourseGrade(int studentId, int courseId);

    List<Integer> getStudentGrades(User user, Course course);

    List<Assignment> getStudentAssignment(User user, Course course);
}
