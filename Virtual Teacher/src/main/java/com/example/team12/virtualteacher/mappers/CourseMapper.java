package com.example.team12.virtualteacher.mappers;

import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.FilterOptionsCourse;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.models.dtos.*;
import com.example.team12.virtualteacher.services.contracts.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
public class CourseMapper {

    private final CourseService courseService;

    @Autowired
    public CourseMapper(CourseService courseService) {
        this.courseService = courseService;
    }

    public Course dtoToObject(User user, CourseDto courseDto, Course course) {
        course.setTitle(courseDto.getTitle());
        course.setTopic(courseDto.getTopic());
        course.setDescription(courseDto.getDescription());
        course.setStartDate(courseDto.getStartDate());
        course.setCreator(user);
        course.setStatus(courseDto.getStatus());

        return course;
    }

    public CourseDtoOut objectToDto(Course course) {
        CourseDtoOut courseDtoOut = new CourseDtoOut();
        courseDtoOut.setTitle(course.getTitle());
        courseDtoOut.setTopic(course.getTopic());
        courseDtoOut.setDescription(course.getDescription());
        courseDtoOut.setStartDate(course.getStartDate());
        courseDtoOut.setCreator(course.getCreator().getFirstName() +
                " " + course.getCreator().getLastName());
        Set<LectureDtoOutForCourse> lectures = new HashSet<>();
        course.getLectures().forEach(lecture -> lectures.add(new LectureDtoOutForCourse(lecture)));
        courseDtoOut.setLectures(lectures);

        return courseDtoOut;

    }

    public FilterOptionsCourse fromFilterDto(CourseFilterDto courseFilterDto) {
        FilterOptionsCourse filterOptionsCourse = new FilterOptionsCourse(
                Optional.ofNullable(courseFilterDto.getTitle()),
                Optional.ofNullable(courseFilterDto.getTopic()),
                Optional.ofNullable(courseFilterDto.getCreator()),
                Optional.ofNullable(courseFilterDto.getRating()),
                Optional.ofNullable(courseFilterDto.getSortBy()),
                Optional.ofNullable(courseFilterDto.getSortOrder())
        );

        return filterOptionsCourse;
    }

    public Course fromCourseEnrolDto(CourseEnrolDto courseEnrolDto) {
        return courseService.getCourseByTitle(courseEnrolDto.getTitle());
    }
}
