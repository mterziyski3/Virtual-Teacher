package com.example.team12.virtualteacher.repositories.contracts;

import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.Rating;
import com.example.team12.virtualteacher.models.User;

public interface RatingRepository extends BaseCRUDRepository<Rating> {

    Rating rate(User user, Course course);

    Double getAvgRating(Course course);

    boolean ifStudentRatedCourse(User user, Course course);

}
