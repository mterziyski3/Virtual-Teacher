package com.example.team12.virtualteacher.repositories.contracts;

import com.example.team12.virtualteacher.models.Assignment;
import com.example.team12.virtualteacher.models.FilterOptionsLecture;
import com.example.team12.virtualteacher.models.Lecture;

import java.util.List;

public interface LectureRepository extends BaseCRUDRepository<Lecture> {

    List<Lecture> filter(FilterOptionsLecture filterOptions);

    Lecture getLectureByTitle(String title);

    void assessAssignment(Assignment assignment);
}
