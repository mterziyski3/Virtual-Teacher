package com.example.team12.virtualteacher.controllers.rest;

import com.example.team12.virtualteacher.controllers.AuthenticationHelper;
import com.example.team12.virtualteacher.exceptions.DuplicateEntityException;
import com.example.team12.virtualteacher.exceptions.EntityNotFoundException;
import com.example.team12.virtualteacher.exceptions.UnauthorizedOperationException;
import com.example.team12.virtualteacher.mappers.UserMapper;
import com.example.team12.virtualteacher.mappers.UserRoleMapper;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.models.UserRole;
import com.example.team12.virtualteacher.models.dtos.RegisterDto;
import com.example.team12.virtualteacher.models.dtos.UserRoleDto;
import com.example.team12.virtualteacher.services.contracts.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/admins")
public class AdminController {
    private final UserMapper userMapper;
    private final UserService userService;
    private final UserRoleMapper userRoleMapper;

    private final AuthenticationHelper authenticationHelper;

    public AdminController(UserMapper userMapper,
                           UserService userService,
                           UserRoleMapper userRoleMapper, AuthenticationHelper authenticationHelper) {
        this.userMapper = userMapper;
        this.userService = userService;
        this.userRoleMapper = userRoleMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @PostMapping
    public User createUser(@RequestBody RegisterDto registerDto) {
        try {
            User user = userMapper.fromRegisterDto(registerDto);
            userService.createUser(user);
            return user;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User userWhoIsDeleting = authenticationHelper.tryGetUser(headers);
            userService.deleteUser(id, userWhoIsDeleting);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());

        }
    }

    @PutMapping("/{id}")
    public void updateRole
            (@RequestHeader HttpHeaders headers,
             @PathVariable int id,
             @RequestBody UserRoleDto userRoleDto) {
        try {
            User userWhoIsGrantingRights = authenticationHelper.tryGetUser(headers);
            UserRole role = userRoleMapper.fromDto(userRoleDto);
            userService.updateUserRole(id, userWhoIsGrantingRights, role);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
