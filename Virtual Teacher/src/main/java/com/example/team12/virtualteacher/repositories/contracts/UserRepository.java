package com.example.team12.virtualteacher.repositories.contracts;

import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.FilterOptionsUser;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.models.UserRole;

import java.util.List;
import java.util.Map;

public interface UserRepository extends BaseCRUDRepository<User> {
    List<User> getUsers();

    List<User> filter(FilterOptionsUser filterOptions);

    void updateUserRole(User userToUpdate, UserRole role);

    List<User> getAllStudents();

    List<Course> getTeacherCreatedCourses(int userId);

    void enrollOnCourse(User user, int courseId);

    User findByVerificationCode(String code);

    User findByResetPasswordToken(String token);

    boolean hasInStudentEnrolledCourses(User user, int courseId);

    List<Course> getStudentCompletedCourses(int studentId);

    Map<String, Double> getStudentCompletedCoursesWithGrades(int studentId);

}
