package com.example.team12.virtualteacher.mappers;

import com.example.team12.virtualteacher.models.FilterOptionsUser;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.models.UserRole;
import com.example.team12.virtualteacher.models.dtos.RegisterDto;
import com.example.team12.virtualteacher.models.dtos.UserDto;
import com.example.team12.virtualteacher.models.dtos.UserFilterDto;
import com.example.team12.virtualteacher.services.contracts.UserService;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserMapper {
    private final UserService userService;


    public UserMapper(UserService userService) {
        this.userService = userService;
    }

    public User fromDto(UserDto userDto) {
        User user = new User();
        dtoToObject(userDto, user);
        return user;
    }

    public User fromDto(UserDto userDto, int id) {
        User user = userService.getUserById(id);
        dtoToObject(userDto, user);
        return user;

    }

    public void dtoToObject(UserDto userDto, User user) {
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        if (userDto.getPassword() == null || userDto.getPassword().isEmpty()) {
            userDto.setPassword(userService.getUserById(userDto.getId()).getPassword());
        }
        user.setPassword(userDto.getPassword());
        if (userDto.getRoleName().equalsIgnoreCase("Student")) {
            UserRole userRole = new UserRole(2, "Student");
            user.setRole(userRole);
        } else if (userDto.getRoleName().equalsIgnoreCase("Teacher")) {
            UserRole userRole = new UserRole(3, "Teacher");
            user.setRole(userRole);
        } else {
            UserRole userRole = new UserRole(1, "Admin");
            user.setRole(userRole);
        }
    }

    public UserDto toDto(User user) {
        UserDto userDto = new UserDto();
        //TODO to check if id can be added
        //  userDto.setId(user.getId());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setPassword(user.getPassword());
        userDto.setRoleName(user.getRole().getName());
        return userDto;
    }


    public User fromRegisterDto(RegisterDto registerDto) {
        User user = new User();
        RegisterDtoToObject(registerDto, user);
        return user;
    }


    public void RegisterDtoToObject(RegisterDto registerDto, User user) {
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setPassword(registerDto.getPassword());
        user.setPassword(registerDto.getPassword());
        user.setEmail(registerDto.getEmail());
        UserRole userRole = new UserRole(2, "Student");
        user.setRole(userRole);
        if (registerDto.getRequestedRoleName().equalsIgnoreCase("Student")) {
            UserRole requestedUserRole = new UserRole(2, "Student");
            user.setRequestedUserRole(requestedUserRole);
        } else if (registerDto.getRequestedRoleName().equalsIgnoreCase("Teacher")) {
            UserRole requestedUserRole = new UserRole(3, "Teacher");
            user.setRequestedUserRole(requestedUserRole);
        }
    }

    public FilterOptionsUser fromFilterDto(UserFilterDto userFilterDto) {
        FilterOptionsUser filterOptionsUser = new FilterOptionsUser(
                Optional.ofNullable(userFilterDto.getFirstName()),
                Optional.ofNullable(userFilterDto.getLastName()),
                Optional.ofNullable(userFilterDto.getEmail()),
                Optional.ofNullable(userFilterDto.getSortBy()),
                Optional.ofNullable(userFilterDto.getSortOrder())
        );

        return filterOptionsUser;

    }
}

