package com.example.team12.virtualteacher.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user_roles")
public class UserRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id")
    private int id;

    @Column(name = "name")
    private String name;

    public UserRole(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public UserRole() {
    }

    public UserRole(String name) {
        setName(name);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserRole)) return false;
        UserRole userRole = (UserRole) o;
        return id == userRole.id && name.equals(userRole.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
