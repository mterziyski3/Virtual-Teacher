package com.example.team12.virtualteacher.controllers.mvc;

import com.example.team12.virtualteacher.controllers.AuthenticationHelper;
import com.example.team12.virtualteacher.exceptions.AuthenticationFailureException;
import com.example.team12.virtualteacher.exceptions.DuplicateEntityException;
import com.example.team12.virtualteacher.exceptions.EntityNotFoundException;
import com.example.team12.virtualteacher.mappers.CommentMapper;
import com.example.team12.virtualteacher.models.Comment;
import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.models.dtos.CommentDto;
import com.example.team12.virtualteacher.services.contracts.CommentService;
import com.example.team12.virtualteacher.services.contracts.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("courses/{courseId}/rate/comments")
public class CommentMvcController {
    private final CourseService courseService;

    private final CommentService commentService;
    private final CommentMapper commentMapper;

    private final AuthenticationHelper authenticationHelper;


    @Autowired
    public CommentMvcController(CourseService courseService,
                                CommentService commentService,
                                CommentMapper commentMapper,
                                AuthenticationHelper authenticationHelper) {
        this.courseService = courseService;
        this.commentService = commentService;
        this.commentMapper = commentMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @PostMapping("/new")
    public String createComment(@PathVariable @Valid int courseId, @ModelAttribute("reply") CommentDto commentDto,
                                BindingResult errors,
                                Model model,
                                HttpSession session) {

        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect/auth/login";
        }
        if (errors.hasErrors()) {
            return "comment-new";
        }
        try {
            Course course = courseService.getById(courseId);
            Comment newComment = commentMapper.dtoToObject(user, course, commentDto, new Comment());
            newComment.setCreator(newComment.getCreator());
            commentService.create(newComment, course, user);
            return String.format("redirect:/");
        } catch (DuplicateEntityException e) {
            errors.rejectValue("content", "comment.exists", e.getMessage());
            return "comment-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }
}
