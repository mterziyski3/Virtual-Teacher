package com.example.team12.virtualteacher.models.dtos;

import com.example.team12.virtualteacher.models.Lecture;

public class LectureDtoOutForCourse {

    private String title;

    private String description;

    public LectureDtoOutForCourse(Lecture lecture) {
        this.title = lecture.getTitle();
        this.description = lecture.getDescription();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
