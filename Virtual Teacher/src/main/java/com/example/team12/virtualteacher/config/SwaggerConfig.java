//package com.example.team12.virtualteacher.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//import springfox.documentation.builders.PathSelectors;
//import springfox.documentation.builders.RequestHandlerSelectors;
//import springfox.documentation.service.ApiInfo;
//import springfox.documentation.service.Contact;
//import springfox.documentation.spi.DocumentationType;
//import springfox.documentation.spring.web.plugins.Docket;
//import springfox.documentation.swagger2.annotations.EnableSwagger2;
//import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;
//
//import java.util.Collections;
//
//@Configuration
//@EnableSwagger2
//@EnableSwagger2WebMvc
//public class SwaggerConfig {
//    @Bean
//    public Docket api() {
//        return new Docket(DocumentationType.SWAGGER_2)
//                .select()
//                .apis(RequestHandlerSelectors.basePackage("com.example.team12.virtualteacher.controllers"))
//                .apis(RequestHandlerSelectors.any())
//                .paths(PathSelectors.any())
//                .build();
//    }
//    private ApiInfo apiDetails(){
//        return new ApiInfo(
//                "Virtual Teacher API",
//                "Online Platform for tutoring",
//                "1.0.O",
//                "",
//                new Contact("Boris Nikolov , Marta Krasteva and Mihail Terziyski", "", ""),
//                "",
//                "",
//                Collections.emptyList());
//    }
//}
//
//// Swagger doc
//// http://localhost:8080/swagger-ui/index.html
