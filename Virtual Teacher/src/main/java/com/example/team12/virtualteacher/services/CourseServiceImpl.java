package com.example.team12.virtualteacher.services;

import com.example.team12.virtualteacher.exceptions.DuplicateEntityException;
import com.example.team12.virtualteacher.exceptions.EntityNotFoundException;
import com.example.team12.virtualteacher.exceptions.UnauthorizedOperationException;
import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.FilterOptionsCourse;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.repositories.contracts.CourseRepository;
import com.example.team12.virtualteacher.services.contracts.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseServiceImpl implements CourseService {


    private static final String CREATE_COURSE_ERROR_MESSAGE = "Only admin or teacher can create a course.";
    private static final String MODIFY_COURSE_ERROR_MESSAGE = "Only admin or teacher can modify a course.";
    private static final String DELETE_REPLY_ERROR_MESSAGE = "Only admin or teacher can delete a course.";
    private final CourseRepository courseRepository;

    @Autowired
    public CourseServiceImpl(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Override
    public List<Course> getAll() {
        return courseRepository.getAll();
    }

    @Override
    public List<Course> getPublished() {
        return courseRepository.getPublished();
    }

    @Override
    public List<Course> filter(FilterOptionsCourse filterOptionsCourse) {
        return courseRepository.filter(filterOptionsCourse);
    }

    @Override
    public List<Course> filterToPublished(FilterOptionsCourse filterOptionsCourse) {
        return courseRepository.filterToPublished(filterOptionsCourse);
    }

    @Override
    public Course getById(int id) {
        return courseRepository.getById(id);
    }

    @Override
    public Course create(Course course, User user) {
        if (!user.checkIfUserTeacher() && !user.checkIfUserAdmin()) {
            throw new UnauthorizedOperationException(CREATE_COURSE_ERROR_MESSAGE);
        }
        boolean duplicateExists = true;
        try {
            courseRepository.getByField("title", course.getTitle());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Title", "title", course.getTitle());
        }
        courseRepository.create(course);
        return course;
    }

    @Override
    public Course update(Course course, User user) {
        if (!user.checkIfUserTeacher() && !user.checkIfUserAdmin()) {
            throw new UnauthorizedOperationException(MODIFY_COURSE_ERROR_MESSAGE);
        }
        boolean duplicateExists = true;
        try {
            Course existingCourse = courseRepository.getByField("title", course.getTitle());
            if (existingCourse.getId() == course.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Title", "title", course.getTitle());
        }
        courseRepository.update(course);
        return course;
    }

    @Override
    public void delete(int id, User user) {
        if (!user.checkIfUserTeacher() && !user.checkIfUserAdmin()) {
            throw new UnauthorizedOperationException(DELETE_REPLY_ERROR_MESSAGE);
        }
        courseRepository.delete(id);
    }

    public Course getCourseByTitle(String title) {
        return courseRepository.getByField("title", title);
    }
}
