package com.example.team12.virtualteacher.services.utils;

public class Constants {

    public static final String CANNOT_ENROLL_BEFORE_SAID_DATE =
            "You cannot enroll in the course before the said date";

    public static final String USER_WITH_THIS_EMAIL_ADDRESS_ALREADY_EXISTS =
            "User with this Email Address already exists.";

    public static final String MODIFY_USER_ERROR_MESSAGE = "Only admin can modify a user.";

    public static final String EDIT_USER_ERROR_MESSAGE = "Only admin or account owners can edit this information.";
    public static final String ADMIN_INFO_ERROR_MESSAGE =
            "Only admins can access this information.";
    public static final String ACCESS_ADMIN_INFO_ERROR_MESSAGE =
            "Only admins or teachers who are account owners can access this information.";
    public static final String ACCESS_TEACHER_INFO_ERROR_MESSAGE =
            "Only admins or teachers can access this information.";
    public static final String ACCESS_STUDENT_INFO_ERROR_MESSAGE =
            "Only admins, teachers or account owner can access this information.";

    public static final String STUDENT_ENROLL_ERROR_MESSAGE =
            "Only students can enroll on courses.";
}
