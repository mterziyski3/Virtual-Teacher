package com.example.team12.virtualteacher.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class LectureDto {
    private int id;

    @NotNull
    @Size(min = 5, max = 50, message = "Title's length should be between 5 and 50 symbols.")
    private String title;

    @NotNull
    @Size(max = 1000, message = "Description's length should be up to 1000 symbols long.")
    private String description;

    private String courseTitle;

    public LectureDto() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCourseTitle() {
        return courseTitle;
    }

    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }
}
