package com.example.team12.virtualteacher.repositories;

import com.example.team12.virtualteacher.models.Assignment;
import com.example.team12.virtualteacher.models.Course;
import com.example.team12.virtualteacher.models.User;
import com.example.team12.virtualteacher.repositories.contracts.AssignmentRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Repository
public class AssignmentRepositoryImpl extends AbstractCRUDRepository<Assignment> implements AssignmentRepository {
    @Autowired
    protected AssignmentRepositoryImpl(SessionFactory sessionFactory) {
        super(Assignment.class, sessionFactory);
    }

    @Override
    public double getStudentAvgCourseGrade(User user, Course course) {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("select avg(grade)"
                    + "from Assignment where lecture.course = :course and user = :user", Double.class);

            query.setParameter("course", course);
            query.setParameter("user", user);
            Double result = query.uniqueResult();
            return BigDecimal.valueOf(result).setScale(1, RoundingMode.HALF_EVEN).doubleValue();
        }
    }


    public List<Integer> getStudentGrades(User user, Course course) {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery
                    ("select grade from Assignment where lecture.course = :course and user = :user" +
                                    " order by lecture.title",
                            Integer.class);
            query.setParameter("course", course);
            query.setParameter("user", user);
            return query.list();
        }
    }

    public List<Assignment> getStudentAssignment(User user, Course course) {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery
                    ("from Assignment where lecture.course = :course and user = :user" +
                                    " order by lecture.title",
                            Assignment.class);
            query.setParameter("course", course);
            query.setParameter("user", user);
            return query.list();
        }
    }

    public boolean studentHasPassedCourse(User user, Course course) {
        Double avgGrade = getStudentAvgCourseGrade(user, course);
        return course.getPassingGrade() <= avgGrade;
    }
}
