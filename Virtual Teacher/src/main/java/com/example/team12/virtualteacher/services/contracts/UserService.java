package com.example.team12.virtualteacher.services.contracts;

import com.example.team12.virtualteacher.models.*;

import java.util.List;
import java.util.Map;

public interface UserService {
    List<User> getUsers(User user);

    User getUserById(int id);

    User createUser(User user);

    User updateUser(User user, User userWhoIsUpdating);

    void deleteUser(int id, User userWhoIsUpdating);

    List<User> filter(FilterOptionsUser filterOptions, User user);

    User getByEmail(String email);

    void updateUserRole(int id, User userWhoIsUpdating, UserRole role);

    List<User> getAllStudents(User user);

    List<Course> getTeacherCreatedCourses(User user, int userId);

    List<Lecture> getStudentEnrolledLectures(User user, int studentId);

    User checkUserById(User user, int id);

    void enrollOnCourse(User user, int courseId);

    List<Course> getStudentCompletedCourses(int studentId);

    boolean verify(String verificationCode);

    Map<String, Double> getStudentCompletedCoursesWithGrades(int studentId);

    void updateResetPasswordToken(String token, String email);

    User getByResetPasswordToken(String token);

    void updatePassword(User user, String newPassword);
}
