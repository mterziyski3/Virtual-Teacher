package com.example.team12.virtualteacher.services;

import com.example.team12.virtualteacher.exceptions.DuplicateEntityException;
import com.example.team12.virtualteacher.exceptions.EntityNotFoundException;
import com.example.team12.virtualteacher.exceptions.UnauthorizedOperationException;
import com.example.team12.virtualteacher.models.*;
import com.example.team12.virtualteacher.repositories.contracts.LectureRepository;
import com.example.team12.virtualteacher.services.contracts.CourseService;
import com.example.team12.virtualteacher.services.contracts.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LectureServiceImpl implements LectureService {
    private static final String MODIFY_LECTURE_ERROR_MESSAGE = "Only admin or creator can modify a lecture.";

    private LectureRepository repository;
    private final CourseService courseService;

    @Autowired
    public LectureServiceImpl(LectureRepository repository, CourseService courseService) {
        this.repository = repository;
        this.courseService = courseService;
    }

    @Override
    public List<Lecture> filter(FilterOptionsLecture filterOptions) {
        return repository.filter(filterOptions);
    }

    @Override
    public List<Lecture> getLectures() {
        return repository.getAll();
    }

    @Override
    public Lecture getLecture(int id) {
        return repository.getById(id);
    }

    @Override
    public Lecture createLecture(Lecture lecture) {
        boolean duplicateLectureExists = true;

        try {
            Lecture existingLecture = repository.getLectureByTitle(lecture.getTitle());
        } catch (EntityNotFoundException e) {
            duplicateLectureExists = false;
        }


        if (duplicateLectureExists) {
            throw new DuplicateEntityException("Lecture", "Title", lecture.getTitle());
        }

        repository.create(lecture);
        return lecture;
    }

    @Override
    public Lecture updateLecture(Lecture lecture, User user) {

        if (!lecture.getCreator().equals(user) && !user.checkIfUserAdmin()) {
            throw new UnauthorizedOperationException(MODIFY_LECTURE_ERROR_MESSAGE);
        }

        boolean duplicateLecture = true;

        try {
            Lecture existingLecture = repository.getLectureByTitle(lecture.getTitle());
            if (lecture.getId() == existingLecture.getId()) {
                duplicateLecture = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateLecture = false;
        }

        if (duplicateLecture) {
            throw new DuplicateEntityException("Lecture", "Title", lecture.getTitle());
        }

        repository.update(lecture);
        return lecture;
    }

    @Override
    public void deleteLecture(int id, User user) {
        Lecture lecture = getById(id);

        Course course = lecture.getCourse();

        if (!course.getCreator().equals(user) && !user.checkIfUserAdmin()) {
            throw new UnauthorizedOperationException(MODIFY_LECTURE_ERROR_MESSAGE);
        }

        repository.delete(id);
    }

    @Override
    public void assessAssignment(Assignment assignment) {
        assignment.setStatusId(1);
        repository.assessAssignment(assignment);
    }

    @Override
    public Lecture getByTitle(String title) {
        return repository.getLectureByTitle(title);
    }

    @Override
    public Lecture getById(int id) {
        return repository.getById(id);
    }
}

