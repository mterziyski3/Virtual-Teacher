INSERT INTO `assigment_statuses` (`assigment_status_id`, `name`)
VALUES (1, 'submitted'),
       (2, 'pending');

INSERT INTO `courses` (`course_id`, `title`, `topic`, `description`, `start_date`, `passing_grade`, `creator_id`,
                       `status`, `rating`)
VALUES;
INSERT INTO `courses_rating` (`student_course_rating_id`, `student_id`, `course_id`, `rating`)
VALUES (4, 9, 10, 4),
       (5, 8, 10, 4),
       (6, 9, 11, 10),
       (7, 8, 9, 4);

INSERT INTO `lectures` (`lecture_id`, `title`, `description`, `course_id`)
VALUES (9, 'Java Kick Start', 'Boost your skills in Java in only 8 minutes', 8),
       (10, 'HTML', 'HTML Introduction', 10),
       (11, 'CSS', 'CSS Introduction', 10),
       (12, 'Conditional Statements', 'Conditional Statements', 10),
       (13, 'Loops with JS', 'Loops with JS', 10),
       (14, 'Arrays with JS', 'Arrays with JS', 10),
       (15, 'Spring Boot', 'Learn Spring Boot', 11),
       (16, 'Hibernate Basics', 'Learn Hibernate Basics', 11),
       (17, 'Thymeleaf Introduction', 'Learn Thymeleaf', 6),
       (18, 'Java Spring Boot Advanced', 'Learn the depths of Spring Boot', 11),
       (19, 'JavaScript Fundamentals', 'Learn the fundamentals of JavaScript', 10);

INSERT INTO `students_courses` (`user_id`, `course_id`)
VALUES (13, 11),
       (13, 10),
       (9, 11),
       (9, 10),
       (16, 11),
       (16, 10),
       (8, 10),
       (4, 9);

INSERT INTO `students_lectures` (`student_lecture_id`, `student_id`, `lecture_id`, `assigment_status_id`,
                                 `assigment_grade`)
VALUES (4, 8, 10, 2, NULL),
       (5, 13, 10, 2, NULL),
       (6, 13, 11, 2, NULL),
       (7, 13, 12, 2, NULL),
       (8, 13, 13, 2, NULL),

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email_address`, `role_id`, `password`, `profile_picture`)
VALUES (3, 'Petar', 'Petrov', 'ppetrov@gmail.com', 3, 'dasdsaads', NULL),
    (4, 'Mihail', 'Terziyski', 'mterziyski@gmail.com', 1, '123123123123', NULL),
    (7, 'Tosho', 'Toshov', 'tosho@abv.bg', 3, '123412341234', NULL),
    (8, 'Todor', 'Toshev', 'ttoshev@gmail.com', 2, '123123123123', NULL),
    (9, 'Dimitar', 'Petrov', 'dimitarp@gmail.com', 2, '123123123123', _binary 0xefbbbf3c6e756c6c3e),
    (12, 'Yordan', 'Todorov', 'yordant@gmail.com', 3, '123123123123', NULL),
    (13, 'Pencho', 'Todorov', 'Penchot@gmail.com', 2, '123123', NULL),
    (14, 'Traicho', 'Traichov', 'traichov@gmail.com', 3, '123123', NULL),
    (15, 'Marta', 'Krasteva', 'marta@abv.bg', 1, 'marta123', NULL),
    (16, 'Maria', 'Krasteva', 'maria@abv.bg', 1, 'maria123', NULL),
    (17, 'Milena', 'Pavlova', 'milena@abv.bg', 1, 'milena123', NULL),

INSERT INTO `user_roles` (`role_id`, `name`)
VALUES (1, 'Admin'),
    (2, 'Student'),
    (3, 'Teacher');

