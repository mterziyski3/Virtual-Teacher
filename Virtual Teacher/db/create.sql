create table assigment_statuses
(
    assigment_status_id int auto_increment
        primary key,
    name                varchar(20) not null,
    constraint assigment_statuses_assigment_status_id_uindex
        unique (assigment_status_id)
);

create table user_roles
(
    role_id int         not null
        primary key,
    name    varchar(20) not null
);

create table users
(
    user_id         int auto_increment
        primary key,
    first_name      varchar(20) not null,
    last_name       varchar(20) not null,
    email_address   varchar(25) not null,
    role_id         int null,
    password        varchar(30) not null,
    profile_picture blob null,
    constraint users_email_address_uindex
        unique (email_address),
    constraint users_user_id_uindex
        unique (user_id),
    constraint users_user_roles_fk
        foreign key (role_id) references user_roles (role_id)
);

create table courses
(
    course_id     int auto_increment
        primary key,
    title         varchar(50)                            not null,
    topic         varchar(30)                            not null,
    description   varchar(1000) null,
    start_date    datetime default '0000-00-00 00:00:00' not null on update current_timestamp (),
    passing_grade int null,
    creator_id    int null,
    status        smallint(15) not null,
    rating        double   default 0 null,
    constraint courses_course_id_uindex
        unique (course_id),
    constraint courses__users_fk
        foreign key (creator_id) references users (user_id)
);

create table courses_rating
(
    student_course_rating_id int auto_increment
        primary key,
    student_id               int null,
    course_id                int null,
    rating                   double(11, 0
) null,
    constraint student_courses_rating_student_course_rating_id_uindex
        unique (student_course_rating_id),
    constraint student_courses_rating_courses_fk
        foreign key (course_id) references courses (course_id),
    constraint student_courses_rating_students_fk
        foreign key (student_id) references users (user_id)
);

create table lectures
(
    lecture_id  int auto_increment
        primary key,
    title       varchar(50) not null,
    description varchar(1000) null,
    course_id   int         not null,
    video       varchar(1000) null,
    constraint lectures_lecture_id_uindex
        unique (lecture_id),
    constraint lectures_course__fk
        foreign key (course_id) references courses (course_id)
);

create table students_courses
(
    user_id   int null,
    course_id int null,
    constraint user_courses_courses_fk
        foreign key (course_id) references courses (course_id),
    constraint user_courses_users_fk
        foreign key (user_id) references users (user_id)
);

create table students_lectures
(
    student_lecture_id  int auto_increment
        primary key,
    student_id          int null,
    lecture_id          int null,
    assigment_status_id int null,
    assigment_grade     int null,
    constraint student_lectures_student_lecture_id_uindex
        unique (student_lecture_id),
    constraint student_lectures_assigment_statuses_fk
        foreign key (assigment_status_id) references assigment_statuses (assigment_status_id),
    constraint student_lectures_students_fk
        foreign key (student_id) references users (user_id),
    constraint students_lectures_lectures_lecture_id_fk
        foreign key (lecture_id) references lectures (lecture_id)
);