# Virtual Teacher


## Project Description

Virtual Teacher is an online platform for tutoring. Users will be able to register as either a teacher or a student. 

Students must be able to enroll for video courses, watch the available video lectures. After finishing each video within a course, students will be asked to submit an assignment, which will be graded by the teacher. After a successful completion of a course, the students must be able to rate it.

Teachers should be able to create courses and upload video lectures with assignments. Each assignment will be graded individually after submission from the students. Users can become teachers only after approval from administrator. 

## Public Section

The public part must be visible without authentication.

Anonymous users must be able to register as students or teachers.

The anonymous users must also be able to browse the catalog of available courses. The user must be able to filter the catalog by course name, course topic, teacher, and rating, as well as to sort the catalog by name and/or rating.

Anonymous users must not be able to enroll for a course.

## Private Part (Students)

Must be accessible only if the student is authenticated.

## Users

Users must have private section (profile page).

Users must be able to:

•   View and edit their profile (names and picture).

•   Change their password.

•   Enroll for courses.

•   See their enrolled and completed courses.

## Courses

•   The videos in a course must be accessible only after enrollment.
•   Students must submit their work as a text file (txt, doc, docx, etc.).
•   Students must be able to see the grade they’ve received for the assignment and their average grade for the course (formed by the average of all assignments grades).
•   After receiving a grade for their last assignment for the course, if their average grade is above the passing grade, defined on per course basis, they should be able to leave a rating for the course

## Private Part (Teachers) 
Teachers must have access to all the functionality that students do.

However, after they are approved by an administrator, a course administration page must be accessible to them. On it they must be able to modify courses and have a section where they can search for students.

Courses must be either drafts or published. Draft courses are visible to the teachers, but not to the students. Once the teacher has prepared the course, they must be able to publish it and it becomes available to all students.

## Administration Part

Administrators must have access to all the functionality that teachers do.

Administrators must not be registered though the ordinary registration process. They can be given administrator rights from the UI by other administrators only.

They must be able to modify/delete courses and users, approve administrators and teachers.

## REST API

Swagger: http://localhost:8080/swagger-ui/index.html

## Technical Requirements

•   Follow OOP principles when coding

•   Follow KISS, SOLID, DRY principles when coding

•   Follow REST API design best practices when designing the REST API (see Appendix)

•   Use tiered project structure (separate the application in layers)

•   The service layer (i.e., "business" functionality) must have at least 80% unit test code coverage

•   Follow BDD when writing unit tests

•   You should implement proper exception handling and propagation

•   Try to think ahead. When developing something, think – “How hard would it be to change/modify this later?”

## Database

The data of the application must be stored in a relational database. You need to identify the core domain objects and model their relationships accordingly. Database structure should avoid data duplication and empty data (normalize your database).
Your repository must include two scripts – one to create the database and one to fill it with data.
 
